#ifndef _CLASSIFIER_ARM_H_
#define _CLASSIFIER_ARM_H_

//#include <iostream>
//#include <algorithm>
//#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>
//#include <boost/shared_ptr.hpp>

#include "classifier.h"
#include "arm/flow_arm.h"
#include "support_arm.h"
//#include "arm/flow_thumb.h"

#include "arm/armulator/armulator.h"

//#include "macros.h"
#define _MIN_INSTR_CHAIN 8
#define MIN_NOP_LEN 80
#define _MAX_INSTR_CHAIN 256
//#define MIN_PATTERN_TRUST 1

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

using namespace std;

//extern boost::shared_ptr<Flow_ARM> flow_ARM;

//extern boost::shared_ptr<Flow_arm> flow_arm;
//extern boost::shared_ptr<Flow_thumb> flow_thumb;
/**
	 * Representation of detected classes fo ARM platform:
	 * 1: plain
	 * 2: decryptor
	 * 3: plain & decryptor
	 */

// APE
const int APE_trust_0 = 0;
const int APE_trust_1 = 0;
const int APE_trust_2 = 20;
const int APE_trust_3 = 30;
const int APE_trust_4 = 40;

// 20 - 1 SVC, 40 - 2 SVC
const int MIN_PATTERN_TRUST = 40;
const int BX_init_trust = 1;
const int CYCLE_TRUST = 1;

const int MAX_BX_GAP = 40;

const int USE_PC_RATE = 4;
const int GET_USE_PC_RATE = 5;

const int MAX_MODE_CHANGE = 1;

const int MAX_wxNum = 8;
const int MAX_unReadNum = 20;
const int MAX_unWriteNum = 20;
const int MAX_unReadWriteNum = 20;

// Number of SVC
const int Malic_call_min = 1;
/*
class RegInit; 
class APE_result; 
class APE_flow_info;
*/
//--------------------------------CLASSIFIERS-----------------------------------//

//========================FLOW BASED ARM PARENT CLASS===========================//
class FlowBased_ARM	
	:public FlowBased {
	
protected:
	//std::bitset<16> regs;
	//int visited[2][MAX_DATA_LEN][MAX_DATA_LEN];
public:
	FlowBased_ARM() {};
	virtual ~FlowBased_ARM() {};
	
	static void init();
	static void init(const unsigned char* _buffer, int data_len);
	static void init_only(const unsigned char* _buffer, int data_len);
	static void reload();
	static void release();
	
	static void init_disassemble();
	static void init_APE();
	static void init_emulator();
	
	virtual bool check(){};
	virtual void printInfo() const {};
};

//=========================FLOW BASED CLASSES===============================================//

/** Class filtering input data with dissasembled chain length less than threshold. */
class DisasLength_ARM
	:public FlowBased_ARM {
	
public:
	int getMinSize_arm(double x);
	int getMinSize_thumb(double x);
	int getChainSize_arm( int i, int j ) const;
	int getChainSize_thumb( int i, int j ) const;

	DisasLength_ARM();

	bool check();
	void printInfo() const { cout << "DisasLength_ARM "; }
};

////=========================APE parent class=========================//
class APE_ARM
	:public FlowBased_ARM {
	
public:
	vector< vector<bool> > visited;
	
	int trust_pattern( bitset<16> regs, bitset<16> regs_has_pc );
	bool decrypt_exist(int i, int s, int j, int mode);
	APE_flow_info forward_flow( int i, int _j, int mode, bitset<16> regs, bitset<16> regs_has_pc );
	
//public:
	APE_ARM();

	bool APE_check();
	virtual void printInfo() const {};
	
	APE_result flow_info;
};

//========================Call patterns init APE CLASSIFIER===================================//
class Call_patterns_init_APE_ARM
	:public APE_ARM {
public:
	Call_patterns_init_APE_ARM();
	
	bool check();
	void printInfo() const { cout << "Call_patterns_init_APE_ARM "; }
};

//========================DisasLength APE CLASSIFIER===================================//
class DisasLength_APE_ARM
	:public APE_ARM {	
public:
	DisasLength_APE_ARM();
	
	bool check();
	void printInfo() const { cout << "DisasLength_APE_ARM "; }
};

//========================BX patterns init APE CLASSIFIER===================================//
class BX_patterns_init_APE_ARM
	:public APE_ARM {
public:
	BX_patterns_init_APE_ARM();
	
	bool check();
	void printInfo() const { cout << "BX_patterns_init_APE_ARM "; }
};

//========================Cycle init APE CLASSIFIER===================================//
class Decrypt_init_APE_ARM
	:public APE_ARM {
public:
	Decrypt_init_APE_ARM();
	
	bool check();
	void printInfo() const { cout << "Decrypt_init_APE_ARM "; }
};

//========================GET USE PC APE CLASSIFIER===================================//
class Get_Use_PC_APE_ARM
	:public APE_ARM {
public:
	Get_Use_PC_APE_ARM();
	
	bool check();
	void printInfo() const { cout << "Get_Use_PC_APE_ARM "; }
};

//========================MODE CHANGE APE CLASSIFIER===================================//
class Mode_change_APE_ARM
	:public APE_ARM {
public:
	Mode_change_APE_ARM();
	
	bool check();
	void printInfo() const { cout << "Mode_change_APE_ARM "; }
};

////=========================APE FLAG CHECK parent class=========================//
class APE_flag_check_ARM
	:public FlowBased_ARM {
	
public:
	vector< vector<bool> > visited;
	
	int trust_pattern( /*bitset<16>*/RegInit regs, /*bitset<16>*/RegInit regs_has_pc, int cond );
	bool decrypt_exist(int i, int s, int j, int mode);
	APE_flow_info_flag forward_flow( int i, int _j, int mode, /*bitset<16>*/RegInit regs, /*bitset<16>*/RegInit regs_has_pc );
	
//public:
	APE_flag_check_ARM();

	bool check();
	void printInfo() const { cout << "APE_flag_check_ARM "; }
	
	APE_result flow_info;
	//int m;
	//vector<pair> storeInd;
};

//========================STRIDE parent class===================================//
class Stride_ARM
	:public FlowBased_ARM{
	
public:
	Stride_ARM();
	
	bool check();
	bool Stride_check(bool instr);
	bool find_sled( int offset, int len, int mode, bool instr );
	bool is_valid_sequence( int offset, int len, int mode );
	
	void printInfo() const { cout << "Stride_ARM "; }
	
	int min_nop_len;
	vector<bool*> visited_off;
};

//========================STRIDE per instruction CLASSIFIER===================================//
/*class Stride_ARM_instr
	:public Stride_ARM{
	
public:
	Stride_ARM_instr();
	
	bool check();
	void printInfo() const { cout << "Stride_ARM_instr "; }
};

//========================STRIDE per byte CLASSIFIER===================================//
class Stride_ARM_byte
	:public Stride_ARM{
	
public:
	Stride_ARM_byte();
	
	bool check();
	void printInfo() const { cout << "Stride_ARM_byte "; }
};*/


//---------------------------------------------------------------------------------------------------//
//=====================================DINAMIC PARENT CLASS==========================================//
class EmulBased_ARM
	:public Classifier{
public:
	EmulBased_ARM();
		
	bool Emul_check(const unsigned char* _buffer, int data_len);
	
	Armulator emul;
	
	bool wxNum, unReadNum, unWriteNum, unReadWriteNum;
	bool branchWrited;
	bool modeChanged;
	bool malicCall;
};



//=====================================wxNum DINAMIC CLASS==========================================//
class wxNum_EmulBased_ARM
	:public Classifier{
public:
	wxNum_EmulBased_ARM();
	
	bool check();
	void printInfo() const { cout << "wxNum_EmulBased_ARM "; }
};
//=====================================decrypt_EmulBased_ARM DINAMIC CLASS==========================================//
class decrypt_EmulBased_ARM
	:public Classifier{
public:
	decrypt_EmulBased_ARM();
	
	bool check();
	void printInfo() const { cout << "decrypt_EmulBased_ARM "; }
};
//=====================================unWriteNum DINAMIC CLASS==========================================//
/*class unWriteNum_EmulBased_ARM
	:public Classifier{
public:
	unWriteNum_EmulBased_ARM();
	
	bool check();
	void printInfo() const { cout << "unWriteNum_EmulBased_ARM "; }
};*/
//=====================================branchWrited DINAMIC CLASS==========================================//
class branchWrited_EmulBased_ARM
	:public Classifier{
public:
	branchWrited_EmulBased_ARM();
	
	bool check();
	void printInfo() const { cout << "branchWrited_EmulBased_ARM "; }
};
//=====================================modeChanged DINAMIC CLASS==========================================//
class modeChanged_EmulBased_ARM
	:public Classifier{
public:
	modeChanged_EmulBased_ARM();
	
	bool check();
	void printInfo() const { cout << "modeChanged_EmulBased_ARM "; }
};
//=====================================malicCall DINAMIC CLASS==========================================//
class malicCall_EmulBased_ARM
	:public Classifier{
public:
	malicCall_EmulBased_ARM();
	
	bool check();
	void printInfo() const { cout << "malicCall_EmulBased_ARM "; }
};











#endif