#include <iostream>
#include <math.h>

#include "classifier.h"

const unsigned char* empty_str=reinterpret_cast<const unsigned char*>("");
int Classifier::data_len=0;
unsigned char* Classifier::_buffer= const_cast<unsigned char*>(empty_str);

int getMinChain(int x)
{
    double res = 7.7351 * pow(10.0, -8.0) * pow((double)x, 3.0) - 0.00023749 * pow( (double)x, 2.0)+ 0.27 * x + 5.9;
    return (int) res;
}

string shellClassToString( int i ) {
	switch(i) {
		case 0:
			return "plain";
		case 1:
			return "nop";
		case 2:
			return "decryptor";
		default:
			return "unknown";
	}
}

//==================== CLASSIFIER PARENT CLASS=================================================//

char Classifier::information_vector; 

void Classifier::init()
{
	information_vector = 0;
   // min_instr_chain = getMinChain(MAX_DATA_LEN - 1);
	
}

Classifier::Classifier()
{
	types_ = 0;
}


void Classifier::reload(const unsigned char *buffer, int length)
{
	_buffer = const_cast<unsigned char*>(buffer);
	data_len = length;
}

void Classifier::updateInformationVector(vector_type type)
{
    char detected_vect = 0;
    if (type == MALWARE)
      detected_vect = types_;
    updateVector(detected_vect);
}

void Classifier::updateVector(char type, double fp_r_prev, double fn_prev)
{
	//TODO: make it correctly with respect to fn and fp
	char cur_v = information_vector & types_;
	if (fn_prev < fn)
	{
	    if(fp_r_prev >= fp_r)
	    {
		cur_v = cur_v & type;
		information_vector = (information_vector & ~types_) | cur_v;
	    }
	}
	else
	{
	    if(fp_r_prev < fp_r)
	    {
	      cur_v = cur_v | (type & types_);
	      information_vector = (information_vector & ~types_) | cur_v;
	    }
	    else
	    {
		cur_v = type & types_;
		information_vector = (information_vector & ~types_) | cur_v;
	    }
	}
	
}

void Classifier::clearInformationVector()
{
	information_vector = 0;
}

bool Classifier::checkInformationVector()
{
	return (information_vector != 0);
}

void Classifier::printInformationVector()
{
	std::cout<< "Information vector: " << information_vector << endl;
}

string Classifier::printClasses()
{
	string classes;

	return classes;
}

void Classifier::featureScaling( double *f_scaled, const double f, 
								const double avr, const double range)
{
		*f_scaled = (f-avr)/range;
}

//====================INITIAL LENGTH CLASSIFIER============================================//

InitialLength::InitialLength()
{
	info = "InitialLength";
	types_=7; //00000111
}

bool InitialLength::check()
{
	PRINT_DEBUG << "in first classifier" << std::endl;
	PRINT_DEBUG << "data_len = " << data_len << " MIN_DATA_LEN =  " << MIN_DATA_LEN << std::endl; 
	return data_len >= MIN_DATA_LEN;
}

//=======================FLOW BASED PARENT CLASS==========================================//
void FlowBased::init()
{
	flow = boost::shared_ptr<Flow>(new Flow(_buffer, data_len));
}

void FlowBased::reload()
{
	init();
}

//=========================CFG BASED PARENT CLASS=========================================//
void CFGBased::init()
{
	s = new State();
	if (!s) {
		return;
	}
	ifg = new EIFG( s );
	if (!ifg) {
		return;
	}
	
	ifg->makeGraph( _buffer, data_len, 0 );
}

void CFGBased::release()
{
	if(s) delete s;
	if(ifg) delete ifg;
}

CFGBased::~CFGBased()
{
}

void CFGBased::reload()
{
	if(s) delete s;
	if(ifg) delete ifg;
	init();
}

//========================DISAS LENGTH CLASSIFIER============================================//
DisasLength::DisasLength()
{	
	fn = 0.05;
	fp_r = 0.67;
	fp_m = 0.95;
	complexity = 12;
	
	info = "DisasLength";
	types_=7; //00000111
}

int DisasLength::maxChainLength() {
    int max_cs=0;
    int cs;

    if(!flow) return -1;

    for (unsigned int i=0; i < flow->getFlowSize(); i++)
    {
        cs = getChainSize(i, 0);
        if (cs > max_cs) max_cs = cs;
    }

    return max_cs;
}

int DisasLength::getChainSize( int i, int j ) const
{
	int size = flow->getChainSize(i); 
	if (size <= 0) return -1;
	
	int offset = flow->getInstructionOffset(i, size-1);

	if ( size - j >= MIN_INSTR_CHAIN || offset < 0 || offset >= MAX_DATA_LEN ||
		  flow->getExternalOffset( offset ) == false ) return size - j;

	int f = flow->getChainNumber(offset);
	int s =flow->getChainOffset(offset);

    //infinite cycle
    if ( i == f ) return size-j;
    size += getChainSize( f, s );
    return size-(j+1);

}

bool DisasLength::check()
{
    int cs;
	if(!flow) return false;

	for (unsigned i = 0; i < flow->getFlowSize(); i++ ) {
		cs = getChainSize( i, 0 );
        if (cs >= min_instr_chain)
		{
			return true;
		}
	}
	return false;
}

//=====================DISAS OFFSET CLASSIFIER=======================================================//
DisasOffset::DisasOffset()
{	
	fn = 0.1;
	fp_r = 0.01;
	fp_m = 0.59;
	complexity = 13;
	
	info = "DisasOffset";
	types_ = 2; //00000010
}

bool DisasOffset::check()
{
	if(!flow) return false;
	
	int prev_off = 0;
	
	for ( int i = 0; i < data_len; i++){
		if (i-prev_off>nop_len) return true;
		if ( flow->offsets[i].first == -1 )
			prev_off = i;
	}
	return false;
}


//======================PUSH-CALL CLASSIFIER======================================================//
PushCall::PushCall()
{	
	fn = 0.15;
	fp_r = 0.02;
	fp_m = 0.77;
	complexity = 790;
	
	info = "PushCall";
	types_ = 7; //00000111
}

int PushCall::pushCallMax(int off, int pcn, bool flag, int* pocn)
{
	std::map< int, Vertex >::iterator v_iter;
	std::map< int, ParCh >::iterator p_iter;	
	int maxpc;

	visited.insert( off );
	
	// such vertex is not exist
	v_iter = ifg->vertexes.find( off );
	if ( v_iter == ifg->vertexes.end() ) return pcn;
	p_iter = ifg->ParentChildArray.find( off );
	if ( p_iter == ifg->ParentChildArray.end() ) return pcn;

	//instruction has no "children". That means that instruction
	// is last in the chain
	if ( (*p_iter).second.child.empty() ) return pcn;


	// PUSH instruction met
	if ( (*v_iter).second.getType() == INSTRUCTION_TYPE_PUSH ){
       // cout << "INSTRUCTION_TYPE_PUSH! " << INSTRUCTION_TYPE_PUSH << endl;
		flag = true;
		(*pocn)++;
	}
	else if ( (*v_iter).second.getType() == INSTRUCTION_TYPE_CALL )
	{
		if ( flag ) pcn++;
		flag = false;
		(*pocn)++;
	}
	maxpc = pcn;
	for ( unsigned int i=0; i < (*p_iter).second.child.size(); i++ ){
		if ( visited.find((*p_iter).second.child[i] ) != visited.end()) break;
		maxpc = std::max( maxpc,	pushCallMax((*p_iter).second.child[i], pcn, flag, pocn));
	}

	visited.erase( off );
	return maxpc;
}

bool PushCall::check()
{
	if(!ifg) return false;
	std::map< int, ParCh >::iterator iter;
	
	for ( iter = ifg->ParentChildArray.begin(); iter!= ifg->ParentChildArray.end(); ++iter){
		// if offset responds to first instruction in some chain
        if( (*iter).second.parent.empty() ){
			visited.clear();
			int pocn=0;
			int pc = pushCallMax((*iter).first, 0, false, &pocn);
			if ( pc >= pushCallThr )
            {
                cout << "pushCall: " << pc << endl;
                return true;
            }
			if(pocn >= pushOrCall)
            {
                cout << "push or call: " << pocn << endl;
                return true;
            }
		}
	}
	return false;
}

//========================DATA FLOW ANOMALY CLASSIFIER==========================================//
DataFlowAnomaly::DataFlowAnomaly()
{	
	fn = 0.21;
	fp_r = 0.02;
	fp_m = 0.77;
	complexity = 724;
	
	int global_range = (MAX_DATA_LEN - MIN_DATA_LEN)/(max_thr - min_thr);
	int local_range = (data_len - MIN_DATA_LEN)/global_range;
	int current_thr = min_thr + local_range; 
	
	if(ifg != NULL && s!=NULL) analyzer = new SGAnalyzer( ifg, s, current_thr);
	
	info = "DataFlowAnomaly";
	types_ = 7;
}

DataFlowAnomaly::~DataFlowAnomaly()
{
	if(analyzer) delete analyzer;
}

bool DataFlowAnomaly::check()
{
	if(!analyzer) return false;
    analyzer->pruningUseless();
    return analyzer->isExecutable();
}

void DataFlowAnomaly::update()
{
	if( analyzer ) delete analyzer;
	
	int global_range = (MAX_DATA_LEN - MIN_DATA_LEN)/(max_thr - min_thr);
	int local_range = (data_len - MIN_DATA_LEN)/global_range;
	int current_thr = min_thr + local_range;
	
	if(ifg != NULL && s!=NULL) analyzer = new SGAnalyzer( ifg, s, current_thr);
	else return;
}

//=============================CYCLE FINDER CLASSIFIER==========================================//
CycleFinder::CycleFinder()
{	
	fn = 0.005;
	fp_r = 0.7;
	fp_m = 0.96;
	complexity = 18;
	
	info = "CycleFinder";
	types_ = 4; //00000100
}

bool CycleFinder::check()
{
	if(!ifg) return false;
	std::map< int, ParCh >::iterator iter;

	for ( iter = ifg->ParentChildArray.begin(); iter!= ifg->ParentChildArray.end(); ++iter){
		// just simple assumption. If some instruction has "child" with 
		// lower offset, than alert cycle presence
		for ( unsigned int i = 0; 
				(*iter).second.child.size(); i++ )
			if ( (*iter).second.child[i] <= (*iter).first )
				return true;
	}
	return false;
}


//=============================RACEWALK CLASSIFIER=============================================//
RaceWalk::RaceWalk()
{	
	fn = 0.21;
	fp_r = 0.35;
	fp_m = 0.25;
	complexity = 81;
	
	racewalk_init(nop_len);
	
	info = "RaceWalk";
	types_ = 2;
}

bool RaceWalk::check()
{
	int l  = data_len;
	int res = racewalk_simple_find_sled( _buffer, l );
	return (res != l);
}

//==========================HDD CLASSIFIER======================================================//

HDD::HDD(int _finderType )
		: finderType(_finderType), emulatorType(1)
{	
	findDecryptor = new FindDecryptor(finderType, emulatorType);
	
	switch (_finderType){
		case 0:
			fp_r = 0.00001;
			fp_m = 0.0;
			fn = 0.21;
			complexity = 489;
			break;
		case 1:
			fp_r = 0.0001;
			fp_m = 0.0;
			fn = 0.17;
			complexity = 371;
			break;
		default:
			fp_r = 0.00001;
			fn = 0.21;
			complexity = 489;
			break;
	}
	
	info = "HDD";
	types_ = 4;
}

HDD::~HDD()
{
	delete findDecryptor;
}

bool HDD::check()
{
	findDecryptor->link(_buffer, data_len);
	return (findDecryptor->find() > 0);
}


