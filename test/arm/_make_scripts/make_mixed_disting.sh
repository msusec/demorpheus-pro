#!/bin/bash

if ! [ $2 ]
then
        echo "incorrect params" 
        exit
fi

if ! [ -d $1 ]
then
        echo "$1 is not a directory" 
        exit
fi

dir=$1
bytes=$2
log=$3

exploits=( $( ls $dir ) )

for i in "${exploits[@]}"
do
        size=`ls -l $dir/$i | awk '{print($5)}'`
        mod=`echo "$bytes-$size%$bytes" | bc`
        cat $dir/$i >> $log/$i
        time dd if=/dev/urandom bs=1 count=$mod >> $log/$i
done













