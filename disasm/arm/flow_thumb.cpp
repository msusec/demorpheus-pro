#include <vector>
#include <map>
#include <iostream>
#include <set>
#include <string.h>

#include "flow_thumb.h"
#include "libdisarm/disarm.h"

#define MAX_INSTRUCTION_LENGTH 16
#define MODE MODE_32

void Flow_thumb::init()
{
		for( int i=0; i < MAX_DATA_LEN; i++){
			offsets[i].first = -1;
			ext_offset[i] = false;
		}
}

Flow_thumb::Flow_thumb()
{
	init();
}
Flow_thumb::Flow_thumb(const unsigned char * buffer, int length )
{
	int o = 0;
	
	init();
	
	while ( length > 0 ){
		//if we didn't disassemble from such offset yet
                if( offsets[o].first == -1 )
					disassemble_flow( buffer+o, length, o );
				o += 1;
                length -= 1;

	}
	return;
}

void Flow_thumb::disassemble_flow(const unsigned char * buffer, unsigned length, int off )
{
	unsigned char ins_str[MAX_INSTRUCTION_LENGTH];
	unsigned char* data;
    da_instr_t 		instr;
	da_instr_args_t args;
	int o = 0;
	int operand_size = THUMB_SIZE;
	std::vector< instruct_thumb > tmp_chain;
	instruct_thumb ins;

	while( length > 1 && off + o < MAX_DATA_LEN )
	{
		//if we didn't dissasemble from such offset yet
        if ( offsets[off+o].first == -1 ){
			//int ins_len = get_instruction(&inst, const_cast<BYTE*>( buffer+o ), MODE);
			//memcpy(&data,buffer+o,ARM_SIZE);
			if ( length > 3 )
			{
				operand_size = 2*THUMB_SIZE;
				data = (unsigned char*)malloc(operand_size);
				memcpy(data,buffer+o,operand_size);
				//printf("%d\n", off+o);
			}
			//filling instruction field
			ins = Lib_wrapper::thumb_get_instr( (const unsigned char*)data, _BIG_ENDIAN, operand_size );
			
			// incorrect dissasembling
			if ( ins.type == _UNDEFINED ) return;
			
			ins.offset = off+o;
			// check jump -> ins.ext_off = true/false;
			
			int off_tmp = fl.size()-1; // the last chain position in the flow

			//if it's first instruction in the given buffer, create new chain in the flow
			//otherwise insert instruction in the end of last chain
			if ( o == 0 ){
				tmp_chain.clear();
				tmp_chain.push_back( ins );
				fl.push_back( tmp_chain );
				offsets[off+o].first = off_tmp+1;
				offsets[off+o].second = fl[ off_tmp+1 ].size()-1;
			}
			else {
				fl[ off_tmp ].push_back( ins );
				offsets[off+o].first = off_tmp;
				offsets[ off+o].second = fl[off_tmp].size()-1;
			}
			length -= ins.size;
			o += ins.size;
        }
		//if we've already met instruction with given offset then create reference element to other 
        // instruction in the other chain ( position of target instruction stored in the offsets vector)
        else {
			ins.offset = off+o;
            ins.ext_off = true;
            ext_offset[off+o] = true;
            fl[ fl.size() -1 ].push_back( ins );
            return;
        }
	}
    return ;

}












