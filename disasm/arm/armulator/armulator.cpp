#include <cstdlib>
#include <iostream>
#include <cstring>
#include "Thumb.h"
#include "error.h"
#include "ARM.h"

#include "armulator.h"

// TODO (Birdman#1#): the default stacktop is at 0x200000, it limits the heap size of 1MB, if more heap spaces is needed, increase the stacktop address,  make the stacktop as a parameter to override the default 0x200000
#pragma align(1)
//char file_name[100] = {0};
Armulator::Armulator()
{
	modeChanged = false;
}

Armulator::~Armulator()
{
	//arm->DeinitMMU();
	//thumb->DeinitMMU();
	//delete arm;
	//delete thumb;
}

int Armulator::execute( const unsigned char* buffer, int length, int mode, int i )
{
	instruct_risc ins;
	bool arm_mode;
	char ch;
	int count=0;
	//cout<<i<<"_"<<mode<<endl;
	wxNum = unReadNum = unWriteNum
	= branchWrited = modeChanged = 0;
	
	_length = length;
	//cout<<_length<<endl; 
	if (mode == 0) {cpu = new ARM;   arm_mode = 1;}
	else		   {cpu = new Thumb; arm_mode = 0;}
	cpu->InitMMU(buffer, length, i);
	
	while( reinterpret_cast<ARM*>(cpu)->rPC < 65536+STACK_JUNK+_length-1 &&//65536
		   reinterpret_cast<ARM*>(cpu)->rPC > -1 &&
		   count < 3000 )
	{
		count++;
		try
		{
			cpu->fetch();
			/*
			printRegs(0); //cout<<endl;
			if (arm_mode) printf("%08i ", reinterpret_cast<ARM*>(cpu)->rPC-4);
			else		  printf("%04i ", reinterpret_cast<Thumb*>(cpu)->rPC-2);
			cout<<" | ";
			if (arm_mode) printf("%08x ", reinterpret_cast<ARM*>(cpu)->cur_instr);
			else		  printf("%04x ", reinterpret_cast<Thumb*>(cpu)->cur_instr);
			
			if (arm_mode) 
			{
				ins.isARM = true;
				ins = Lib_wrapper::arm_get_instr( reinterpret_cast<ARM*>(cpu)->cur_instr, 
																_LITTLE_ENDIAN );
			}
			else		  
			{
				ins.isARM = false;
				ins = Lib_wrapper::thumb_get_instr( (const unsigned char*)&(reinterpret_cast<Thumb*>(cpu)->cur_instr), 
																_BIG_ENDIAN, 2 );
			}
			
			ins.printInstr(); cout<<endl;
			*/
			cpu->exec(); //gets(&ch);
		}
		catch(Error &e)
		{
			//std::cout<<"\nError:"<<e.error_name<<std::endl;
			break;
		}
		catch(UnexpectInst &e)
		{
			//std::cout<<"\nUnexpect Instr:"<<e.error_name<<std::endl;
			break;
		}
		catch(UndefineInst &e)
		{
			//std::cout<<"\nUndefine Instr:"<<e.error_name<<std::endl;
			break;
		}
		catch(SwitchMode &e)
		{
			modeChanged = true;
			if (arm_mode)
			{
				Thumb *tmp = new Thumb;
				tmp->CopyCPU(cpu);
				delete cpu;
				cpu = tmp;
				arm_mode = false;
			}
			else
			{
				ARM *tmp = new ARM;
				tmp->CopyCPU(cpu);
				delete cpu;
				cpu = tmp;
				arm_mode = true;
			}
		}
		catch(ProgramEnd &e)
		{
			//std::cout<<"\nThe Program Ended\n";
			break;
		}
	}
	
	wxNum = reinterpret_cast<ARM*>(cpu)->my_mmu->wxNum;
	unReadNum = reinterpret_cast<ARM*>(cpu)->my_mmu->unReadNum;
	unWriteNum = reinterpret_cast<ARM*>(cpu)->my_mmu->unWriteNum;
	branchWrited = reinterpret_cast<ARM*>(cpu)->branchWrited;
	malicCall = reinterpret_cast<ARM*>(cpu)->malicCall;
	
	cpu->DeinitMMU();
	delete cpu;
	
	return count;
}

void Armulator::printRegs( bool arm_mode )
{
	cout<<"                                                      ";
	if (arm_mode)
		for (int i=0; i<16; i++)
		{
			cout<<reinterpret_cast<ARM*>(cpu)->r[i]<<"_";
		}
	else
		for (int i=0; i<16; i++)
		{
			cout<<reinterpret_cast<Thumb*>(cpu)->r[i]<<"_";
		}
	cout<<endl;
}

/*!
	entry point of the emulator, pass the parameters into the Thumb program through this function. Start the emulator.
	\param param_1 first parameter to be passed
	\param param_2 second parameter to be passed
	\return The result of the running program
 */
 /*
int azaza_main(int argc,char* argv[])
{
    //char main_param[100] = {0};
    // format the parameter, seperate the parameter by 0x20
    //sprintf(main_param, "%d\040%d\040", param_1, param_2);

	if (argc != 2)
	{
		std::cout<<"Use: \"ARMulator [file name]\" to run!"<<std::endl;
		return EXIT_FAILURE;
	}
	
	strcpy(file_name, argv[1]);
	
    CPU *arm = new ARM;

    try
    {
        arm->InitMMU();
    }
    catch(Error &e)
    {
        std::cout<<"\nError:"<<e.error_name<<std::endl;
        return EXIT_FAILURE;
    }


    while(1)
    {
        try
        {
            arm->fetch();
            arm->exec();
        }
        catch(Error &e)
        {
            arm->DeinitMMU();
            std::cout<<"\nError:"<<e.error_name<<std::endl;
            break;
        }
        catch(UnexpectInst &e)
        {
            std::cout<<"\nUnexpect Instr:"<<e.error_name<<std::endl;
            break;
        }
        catch(UndefineInst &e)
        {
            std::cout<<"\nUndefine Instr:"<<e.error_name<<std::endl;
            break;
        }
        catch(SwitchMode &e)
        {
            Thumb *tmp = new Thumb;
            tmp->CopyCPU(arm);
            delete arm;
            arm = tmp;
            //arm->getArg(main_param, strlen(main_param));
        }
        catch(ProgramEnd &e)
        {
            std::cout<<"\nThe Program Ended\n";
            break;
        }
    }

    arm->DeinitMMU();
    delete arm;


    return EXIT_SUCCESS;
}
*/