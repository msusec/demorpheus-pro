#include <vector>
#include <map>
#include <iostream>
#include <set>
#include <string.h>

#include "flow_arm.h"
#include "libdisarm/disarm.h"

#define MAX_INSTRUCTION_LENGTH 16
#define MODE MODE_32

using namespace std;

// ************************** Parent class **************************
Flow_ARM::Flow_ARM()
{
	
}

Flow_ARM::~Flow_ARM()
{
	
}

bool Flow_ARM::check_complex_payload(instruct_risc ins)
{
	switch (ins.type){
	
	case BRANCH_LINK_EXCHANGE:
		if ( ins.visible_branch_address )
		{
			if ( ins.branch_offset < MAX_JUMP && ins.branch_offset > -1*MAX_JUMP )
				return true;
			else
				return false;
		}
		else
		{
			return true;
		}
		break;
	
	case BRANCH_EXCHANGE:
		return true;
		break;
	
	case BRANCH_LINK:
		if ( ins.branch_offset < MAX_JUMP && ins.branch_offset > -1*MAX_JUMP )
			return true;
		else
			return false;
		break;
	
	case BRANCH:
		if ( ins.branch_offset < MAX_JUMP && ins.branch_offset > -1*MAX_JUMP )
			return true;
		else
			return false;
		break;
	
	case SVC:
		return true;
		break;
	
	default:
		if ( ins.change_pc )
			return true;
			
		return false;
		break;
	}	
	return false;
}
// ************************** ARM **************************
Flow_arm::~Flow_arm()
{
	
}

void Flow_arm::init()
{
		for( int i=0; i < MAX_DATA_LEN; i++){
			offsets[i].first = -1;
			ext_offset[i] = false;
		}
}

Flow_arm::Flow_arm()
{
	init();
}
Flow_arm::Flow_arm(const unsigned char * buffer, int length )
{
	int o = 0;
	
	init();
	
	//while ( length > 0 ){
	for ( int i=0; i<4; i++ ){
		//if we didn't disassemble from such offset yet
                if( offsets[i].first == -1 )
					disassemble_flow( buffer+i, length, i );
				o += 1;
                length -= 1;

	}
	return;
}

int Flow_arm::disassemble_flow(const unsigned char * buffer, unsigned length, int off )
{
	unsigned char ins_str[MAX_INSTRUCTION_LENGTH];
	da_word_t 		data;
    da_instr_t 		instr;
	da_instr_args_t args;
	int o = 0;
	std::vector< /*instruct_arm*/instruct_risc > tmp_chain;
	/*instruct_arm*/instruct_risc ins;
	bool complex_payload = false;
//cout<<"---------- NEW ARM -----------"<<fl.size()<<endl;
	while( length > 3 && off + o < MAX_DATA_LEN )
	{//cout<<complex_payload<<"  offset= "<<off + o<<" cur_of= "<<o<<endl;
		//if we didn't dissasemble from such offset yet
        if ( offsets[off+o].first == -1 ){
			memcpy(&data,buffer+o,ARM_SIZE);
			
			//filling instruction field
			ins = Lib_wrapper::arm_get_instr( data/**(buffer+o)*/, /*_BIG_ENDIAN*/_LITTLE_ENDIAN );
			//cout<<" | "; ins.printType(); /*ins.printInstr();*/ cout<<endl;
			//cout<<ins.type<<endl;
			if ( check_complex_payload(ins) )
			{
				complex_payload = true;
			}
		
			// incorrect dissasembling
			if ( ins.type == _UNDEFINED || ins.type == _INCORRECT ) 
			{
				//undef_num++;
				//return off+o;
				if (fl.size()!=0)
				if ( !complex_payload /*|| (!complex_payload || fl[fl.size()-1].size() < MIN_DISAS_LEN)*/ && o != 0 )
					{fl.pop_back();}
				
				/*!!!!!if ( length-ARM_SIZE > 3 )
					disassemble_flow( buffer+o+ARM_SIZE, length-ARM_SIZE, off+o+ARM_SIZE );*/
				//cout<<"===END ARM==="<<fl.size()<<endl;
				return off+o;
			}
			
			ins.isARM = true;
			ins.offset = off+o;
			// check jump -> ins.ext_off = true/false;
			
			int off_tmp = fl.size()-1; // the last chain position in the flow
			ins.ext_off = false;
			//if it's first instruction in the given buffer, create new chain in the flow
			//otherwise insert instruction in the end of last chain
			if ( o == 0 ){
				tmp_chain.clear();
				tmp_chain.push_back( ins );
				fl.push_back( tmp_chain );
				offsets[off+o].first = off_tmp+1;
				offsets[off+o].second = fl[ off_tmp+1 ].size()-1;
			}
			else {
				fl[ off_tmp ].push_back( ins );
				offsets[off+o].first = off_tmp;
				offsets[off+o].second = fl[off_tmp].size()-1;
			}
			length -= ins.size;
			o += ins.size;
        }
		//if we've already met instruction with given offset then create reference element to other 
        // instruction in the other chain ( position of target instruction stored in the offsets vector)
        else {
			ins.offset = off+o;
            ins.ext_off = true;
            ext_offset[off+o] = true;
            fl[ fl.size() -1 ].push_back( ins );
			
			if (fl.size()!=0)
			if ( !complex_payload /*|| (!complex_payload || fl[fl.size()-1].size() < MIN_DISAS_LEN)*/ && o != 0 )
				{fl.pop_back();}
			
			/*!!!!!if ( length-ARM_SIZE > 3 )
					disassemble_flow( buffer+o+ARM_SIZE, length-ARM_SIZE, off+o+ARM_SIZE );*/
			/*if ( complex_payload && o != 0 )
				{fl.pop_back();}*/
            //cout<<"===END-AZAZ ARM==="<<fl.size()<<endl;
			return off + o;
        }
	}
	if (fl.size()!=0)
	if ( !complex_payload /*|| (!complex_payload || fl[fl.size()-1].size() < MIN_DISAS_LEN)*/ && o != 0 )
		{fl.pop_back();}
    //cout<<"===END ARM==="<<fl.size()<<endl;
	return off+o;

}

// ************************** Thumb **************************
Flow_thumb::~Flow_thumb()
{
	
}

void Flow_thumb::init()
{
		for( int i=0; i < MAX_DATA_LEN; i++){
			offsets[i].first = -1;
			ext_offset[i] = false;
		}
}

Flow_thumb::Flow_thumb()
{
	init();
}
Flow_thumb::Flow_thumb(const unsigned char * buffer, int length )
{
	int o = 0;
	
	init();
	
	//while ( length > 0 ){
	for ( int i=0; i<2; i++ ){
		//if we didn't disassemble from such offset yet
                if( offsets[i].first == -1 )
					disassemble_flow( buffer+i, length, i );
				o += 1;
                length -= 1;

	}
	return;
}

int Flow_thumb::disassemble_flow(const unsigned char * buffer, unsigned length, int off )
{
	unsigned char ins_str[MAX_INSTRUCTION_LENGTH];
	//unsigned char* data;
	unsigned int data;
    da_instr_t 		instr;
	da_instr_args_t args;
	int o = 0;
	int operand_size = 2*THUMB_SIZE;
	std::vector< /*instruct_thumb*/instruct_risc > tmp_chain;
	/*instruct_thumb*/instruct_risc ins;
	bool complex_payload = false;
//cout<<"---------- NEW THUMB-----------"<<fl.size()<<endl;
	while( length > 1 && off + o < MAX_DATA_LEN )
	{//cout<<complex_payload<<"  offset= "<<off + o<<" cur_of= "<<o<<endl;
		//if we didn't dissasemble from such offset yet
        if ( offsets[off+o].first == -1 ){
			
			if ( length > 3 )
			{
				operand_size = 2*THUMB_SIZE;
			}
			else
			{
				operand_size = THUMB_SIZE;
			}
			//data = (unsigned char*)malloc(operand_size);
			//memcpy(&data,buffer+o,operand_size);
			
			if ( length < 4 )
				operand_size = THUMB_SIZE;
			//filling instruction field
			//ins = Lib_wrapper::thumb_get_instr( (const unsigned char*)&data, _BIG_ENDIAN, operand_size );
			ins = Lib_wrapper::thumb_get_instr( (const unsigned char*)buffer+o, _BIG_ENDIAN, operand_size );
			//cout<<" | "; ins.printType(); /*ins.printInstr();*/ cout<<endl;
			//cout<<ins.type<<endl;
			if ( check_complex_payload(ins) )
			{
				complex_payload = true;
			}
			
			// incorrect dissasembling
			if ( ins.type == _UNDEFINED || ins.type == _INCORRECT )
			{
				//undef_num++;
				//return off+o;
				if (fl.size()!=0)
				if ( !complex_payload /*|| (!complex_payload && fl[fl.size()-1].size() < MIN_DISAS_LEN)*/ && o != 0 )
					{fl.pop_back();}
				
				/*!!!!!if ( length-THUMB_SIZE > 1 )
					disassemble_flow( buffer+o+THUMB_SIZE, length-THUMB_SIZE, off+o+THUMB_SIZE );*/
				//cout<<"===END THUMB==="<<fl.size()<<endl;
				return off+o;
			} 
			
			ins.isARM = false;
			ins.offset = off+o;
			// check jump -> ins.ext_off = true/false;
			
			int off_tmp = fl.size()-1; // the last chain position in the flow
			ins.ext_off = false;
			//if it's first instruction in the given buffer, create new chain in the flow
			//otherwise insert instruction in the end of last chain
			if ( o == 0 ){
				tmp_chain.clear();
				tmp_chain.push_back( ins );
				fl.push_back( tmp_chain );
				offsets[off+o].first = off_tmp+1;
				offsets[off+o].second = fl[ off_tmp+1 ].size()-1;
			}
			else {
				fl[ off_tmp ].push_back( ins );
				offsets[off+o].first = off_tmp;
				offsets[ off+o].second = fl[off_tmp].size()-1;
			}
			length -= ins.size;
			o += ins.size;
        }
		//if we've already met instruction with given offset then create reference element to other 
        // instruction in the other chain ( position of target instruction stored in the offsets vector)
        else {
			ins.offset = off+o;
            ins.ext_off = true;
            ext_offset[off+o] = true;
            fl[ fl.size() -1 ].push_back( ins );
			
			if (fl.size()!=0)
			if ( !complex_payload /*|| (!complex_payload || fl[fl.size()-1].size() < MIN_DISAS_LEN)*/ && o != 0 )
				{fl.pop_back();}
			
			/*!!!!!if ( length-THUMB_SIZE > 1 )
					disassemble_flow( buffer+o+THUMB_SIZE, length-THUMB_SIZE, off+o+THUMB_SIZE );*/
			
			/*if ( complex_payload && o != 0 )
				{fl.pop_back();}*/
			//cout<<"===END-AZAZ THUMB==="<<fl.size()<<endl;
            return off+o;
        }
	}
	if (fl.size()!=0)
	if ( !complex_payload /*|| (!complex_payload || fl[fl.size()-1].size() < MIN_DISAS_LEN)*/ && o != 0 )
		{fl.pop_back();}
    //cout<<"===END THUMB==="<<fl.size()<<endl;
	return off+o;

}
/*
void Flow_ARM::printFlow()
{
	for(int m=0; m<=1; m++)
		{
			cout<<"___________STATE = "<<m<<" ___________"<<endl;
			for(int i=0; i<flow_ARM->at(m)->fl.size(); i++)
			{
				cout<<"___i = "<<i<<endl;
				for(int j=0; j<flow_ARM->at(m)->fl[i].size(); j++)
					{
						flow_ARM->at(m)->fl[i][j].printType();cout<<endl;
					}
			}
			cout<<endl;
		}
}
*/









