#ifndef _EIFG_H
#define _EIFG_H

#include <iostream>
#include <map>
#include <iterator>
#include <string.h>
#include <vector>
#include <utility>
#include "state.h"
#include "macros.h"

#define PUSH_CALL_THR 20

using namespace std;

typedef unsigned char* xbyte;

class Vertex
{
	int type:8;
	bool jmp;
	BYTE name;
	int size;
	int offset;
	bool external;
	int op1;

public:
	std::vector <int> parent;
	std::vector <int> child;

	Vertex():type(0), jmp(false), size(0), offset(0), external(false), op1(-1) {}
	~Vertex(){};

	int getType () const  { return type;  }
	bool isJmp () const { return jmp; }
	unsigned char* getName () const { return reinterpret_cast<BYTE*>(name); }
	int getSize () const { return size; }
	int getOffset () const { return offset; }
	bool getExternal () const { return external; }
	int getOp1() const { return op1; }

	Vertex& setType ( int t ) { type = t; return *this; }
	Vertex& setJmp ( bool j = true ) { jmp = j; return *this; }
	Vertex& setName ( BYTE  n ) 
	{
		name = n;
		return *this;
	}
	Vertex& setSize ( int s ) { size = s; return *this; }
	Vertex& setOffset ( int o ) { offset = o; return *this; }
	Vertex& setExternal ( bool fl = true ) { external = fl; return *this; }
	Vertex& setParent ( int o ) { parent.push_back ( o ); return *this; }
	Vertex& setOp1 ( int reg ) { op1 = reg; return *this; }
	
	//! function fills vertext field with given instruction
	/**
	 * @param inst INSTRUCTION - disassembled instruction
	 * @param len int - length of disassembled instruction
	 * @param o int  - offset from buffer beginning of disassembled instruction
	 */
	Vertex& fillVertex ( INSTRUCTION inst, int len, int o );

	void printVertex () 
	{
		cout << "valid = " << type << " jmp = " << jmp;
		cout << " name = " << *(&name) << " size = " << size << " offset = " << offset << std::endl;
	}


	friend class EIFG;  
};

typedef std::pair< int , int > Pair;
typedef std::pair< int, bool > inst_Pair;

static std::map < xbyte, inst_Pair > InstructionArray;

struct ParCh
{
	std::vector< int > parent;
	std::vector< int > child;
};

class EIFG;

class EIFG
{
	//! Array of external vertexes.
	/**
	 * Some vertex is added to external vertexes array if there was met transfer of control flow to this address,
	 * but vertex with this addres isn't exist yet
	 * key - offset of external instruction
	 * vector<int> - vector of instruction addresses which have redirection to external instruction
	 */
	std::map < int , std::vector < int > > ExtArray;

	int push_call_cnt;
	
private: 
	
	//!
	/**
	 * 
	 */
	EIFG& libdasmGenerator (const unsigned char* buffer, unsigned lenght, int o  );	
	
	//!function checks if instruction which begins from given address are in array of external variables
	/**
	 * @param @addr  int - address of given instruction
	 * @return true if address exists in array of external elements
	 */
	bool isExt ( int addr );
	
	//! function updates relations between two instructions in control flow
	/**
	 * Function updates ParentChildArray: it inserts second instruction as child of first
	 * and first as parent of second
	 * @param parent int - address of instruction which has redirection to child
	 * @param child int  - address of instruct to which parent has redirection
	 */
	void insertChildAndParent( int parent, int child );
	
	//! function inserts edges which corresponding to JMP instruction
	/**
	 * Function insert edges between JMP instruction and its target. If target is not external, 
	 * function updates relations between two instructions. Otherwise finctions adds JMP target
	 * to array of external instructions
	 * @param instruction_offset int  - address of JMP instruction
	 * @param jmp_target int - address of JMP instruction target
	 */
	void processJmpInstruction(int instruction_offset, int jmp_target);
	
	//! function processing specific case of instruction types: JMP instructions and define instructions
	/**
	 * @param inst INSTRUCTION - disassembled instruction
	 * @param o int - offset of considering instruction
	 */
	bool processSpecificInstruction(INSTRUCTION inst, int o);
public:
	EIFG ( State* s );
	~EIFG();
	
	friend class Vertex;
	
	std::map< int, Vertex > vertexes;
	
	//! array contains information about parents and childrens of vertex
	std::map< int, ParCh > ParentChildArray;
	State* variablesState; 
	
	//! wrapper class that generates ifg from given buffer
	/**
	 * Class generates ifg from given buffer.
	 * @param buffer  - given buffer from which ifg will bw constructed
	 * @param length - the length of given buffer
	 * @param o - offset from which to start disassembly process in buffer
	 * @return EIFG  - constructed instruction flow graph
	 */
	EIFG& makeGraph ( const unsigned char* buffer, int length, int inst_offset );
	
	//vertex
	EIFG& insertVertex( Vertex* v );
	Vertex& getExternal ( int o );
	bool findVertex ( int o );
	bool findVertex ( Vertex* v);
	int getVertexType ( int o );
	int getVertexOp1 ( int o );
	Vertex* getVertex( int o );
	EIFG& removeVertex( int o );
	void deleteFamilyRelations( int o );

	//edge
	EIFG& removeRelativeEdges( int o );
	EIFG& removeEdge ( int o1, int o2 );

	//print information	
	void printInformation();
	void printVertexes();
	int getVertexNumber();


	//eifg generation
	void doJmp ( int o, unsigned char* buffer);
	void doExt ( int o );
	void insertJmp (int addr, Vertex* v);
	void removeJmp (int addr);
	bool isAddr (int addr);
	int getNodeOffset (int addr);
	EIFG& generator ( const unsigned char* buffer, unsigned lenght, int o  );
	
	void clearGraph();
	int pushNum ( int o );

	//useless prunning
	EIFG& addRelativeEdges( int o );

};


#endif
