include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}/
        ${CMAKE_CURRENT_SOURCE_DIR}/tests/
        ${SHELLCODE_DIR}/disasm
		${SHELLCODE_DIR}/
)
set (	SigFree_SRC
	eifg.h
	eifg.cpp
	state.h
	state.cpp
	analyzer.h
	analyzer.cpp
	${SHELLCODE_DIR}/macros.h
)

add_library( SigFree SHARED ${SigFree_SRC} )
set_target_properties( SigFree PROPERTIES COMPILE_FLAGS "-g -Wno-sign-compare" )
target_link_libraries( SigFree libdasm )

enable_testing()

find_package(Boost REQUIRED)
add_subdirectory(tests tests)
#add_executable(eifg_test tests/eifg_test.cpp)
#add_test( eifg_test tests/eifg_test.cpp)

#add_executable(test test.cpp)
#target_link_libraries(test SigFree)
