#ifndef _SUPPORT_ARM_H_
#define _SUPPORT_ARM_H_

#include <bitset>

//#include "classifier_arm.h"


//======================================= SUPPORT ============================================


class RegInit{
public:
	RegInit();
	
	void Sbit();
	void reset(int cond=15);
	void ALcond( int cond );
	//bool read_with_ALcond(int reg);
	std::bitset<16>& operator[]( int cond );
	std::bitset<16> get( int cond );
	
	std::bitset<16> regs[15];
	std::bitset<16> tmp;
	
	int m;
};

class APE_result{
public:
	APE_result() { clear(); }
	void clear() 
	{
		disas_len = patterns = BX_num
		= decrypt_exist = getpc = mode_change = false;
	}
	bool is_full()
	{
		
	}
	
	bool disas_len;
	bool patterns;
	bool BX_num;
	bool decrypt_exist;
	bool getpc;
	bool mode_change;
};

class APE_flow_info{
public:
	int disas_len, max_disas_len;
	
	int patterns_trust;
	int patterns_num;
	int patterns_init;
	
	int BX_number;
	int BX_patterns;
	
	int svc_num;
	
	int cycle_number;
	
	int getpc, usepc, branch_use_pc;
	int load_num, store_num;
	
	int mode_change;
	
	std::bitset<16> regs;
	std::bitset<16> regs_has_pc;
	
	APE_flow_info()
	{
		disas_len = patterns_trust = max_disas_len = BX_number = BX_patterns = cycle_number =
		getpc = usepc = mode_change = branch_use_pc = load_num = store_num = svc_num = 0;
		//regs[13] = regs[14] = regs[15] = 1;
	}
	APE_flow_info& operator+=(const APE_flow_info& second)
	{
		max_disas_len += second.max_disas_len;
		
		patterns_trust += second.patterns_trust;
		patterns_num += second.patterns_num;
		patterns_init += second.patterns_init;
		
		BX_number += second.BX_number;
		BX_patterns += second.BX_patterns;
		
		svc_num += second.svc_num;
		
		cycle_number += second.cycle_number;
		
		getpc += second.getpc;
		usepc += second.usepc;
		branch_use_pc += second.branch_use_pc;
		load_num += second.load_num;
		store_num += second.store_num;
		
		mode_change += second.mode_change;
	}
	void inc_disas_len()
	{
		disas_len++;
		if ( disas_len > max_disas_len ) max_disas_len = disas_len;
	}
};


class APE_flow_info_flag{
public:
	int disas_len, max_disas_len;
	
	int patterns_trust;
	int patterns_num;
	int patterns_init;
	
	int BX_number;
	int BX_patterns;
	
	int svc_num;
	
	int cycle_number;
	
	int getpc, usepc, branch_use_pc;
	int load_num, store_num;
	
	int mode_change;
	
	RegInit regs;
	RegInit regs_has_pc;
	
	APE_flow_info_flag()
	{
		disas_len = patterns_trust = max_disas_len = BX_number = BX_patterns = cycle_number =
		getpc = usepc = mode_change = branch_use_pc = load_num = store_num = svc_num = 0;
		//regs[13] = regs[14] = regs[15] = 1;
	}
	APE_flow_info_flag& operator+=(const APE_flow_info_flag& second)
	{
		max_disas_len += second.max_disas_len;
		
		patterns_trust += second.patterns_trust;
		patterns_num += second.patterns_num;
		patterns_init += second.patterns_init;
		
		BX_number += second.BX_number;
		BX_patterns += second.BX_patterns;
		
		svc_num += second.svc_num;
		
		cycle_number += second.cycle_number;
		
		getpc += second.getpc;
		usepc += second.usepc;
		branch_use_pc += second.branch_use_pc;
		load_num += second.load_num;
		store_num += second.store_num;
		
		mode_change += second.mode_change;
	}
	void inc_disas_len()
	{
		disas_len++;
		if ( disas_len > max_disas_len ) max_disas_len = disas_len;
	}
};


#endif